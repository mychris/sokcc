/* SPDX-License-Identifier: MIT */
#include "algorithms.h"
#include "carray.h"
#include "debug.h"
#include "level.h"
#include <cstring>
#include <linux/limits.h>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

State& State::operator=(const State& o)
{
  if (this == &o)
    return *this;

  std::copy(o.CratePositions, o.CratePositions + o.NumberOfCrates, CratePositions);

  NumberOfCrates = o.NumberOfCrates;
  PlayerPosition = o.PlayerPosition;
  return *this;
}

State& State::operator=(State&& o) noexcept
{
  PlayerPosition = o.PlayerPosition;
  NumberOfCrates = o.NumberOfCrates;
  std::swap(CratePositions, o.CratePositions);
  return *this;
}

bool State::operator==(const State& o) const
{
  if (PlayerPosition != o.PlayerPosition)
    return false;
  if (NumberOfCrates != o.NumberOfCrates)
    return false;
  return std::memcmp(CratePositions, o.CratePositions, sizeof(size_t) * NumberOfCrates) == 0;
}

bool State::operator!=(const State& o) const
{
  return !(*this == o);
}

Level::Level()
  : Width(0)
  , Height(0)
  , Length(0)
  , PlayerPosition(0, 0)
  , NumberOfCrates(0)
  , CratePositions(nullptr)
  , ReachablePositions()
  , ReachablePositionsQueue(nullptr)
  , ReachablePositionsValid(false)
  , ShortestPathCache()
  , ShortestPathQueue(nullptr)
  , Data()
{
}

Level::Level(size_t width, size_t height, LElement* data)
  : Width(width)
  , Height(height)
  , Length(width * height)
  , PlayerPosition(0, width * height)
  , NumberOfCrates(0)
  , CratePositions(nullptr)
  , ReachablePositions(width * height)
  , ReachablePositionsQueue(new LPos[width * height]())
  , ReachablePositionsValid(false)
  , ShortestPathCache(width * height)
  , ShortestPathQueue(new LPos[width * height]())
  , Data(data, width * height)
{
  for (const auto& pos : LPosRange(LPos(0, Length), Length)) {
    if (Data[pos] & LElement::Player) {
      PlayerPosition = pos;
    }
    if (Data[pos] & LElement::Crate) {
      ++NumberOfCrates;
    }
  }
  CratePositions = new LPos[NumberOfCrates];
  size_t crate = 0;
  for (const auto& pos : LPosRange(LPos(0, Length), Length)) {
    if (Data[pos] & LElement::Crate) {
      CratePositions[crate] = pos;
      ++crate;
    }
  }
}

Level::~Level()
{
  delete[] CratePositions;
  delete[] ReachablePositionsQueue;
  delete[] ShortestPathQueue;
}

Level::Level(const Level& o)
  : Width(o.Width)
  , Height(o.Height)
  , Length(o.Length)
  , PlayerPosition(o.PlayerPosition)
  , NumberOfCrates(o.NumberOfCrates)
  , CratePositions(new LPos[o.NumberOfCrates])
  , ReachablePositions(o.Length)
  , ReachablePositionsQueue(new LPos[o.Length]())
  , ReachablePositionsValid(false)
  , ShortestPathCache(o.Length)
  , ShortestPathQueue(new LPos[o.Length]())
  , Data(o.Data)
{
  std::copy(o.CratePositions, o.CratePositions + o.NumberOfCrates, CratePositions);
}

Level::Level(Level&& o) noexcept
  : Width(o.Width)
  , Height(o.Height)
  , Length(o.Length)
  , PlayerPosition(o.PlayerPosition)
  , NumberOfCrates(o.NumberOfCrates)
  , CratePositions(std::exchange(o.CratePositions, nullptr))
  , ReachablePositions(std::move(o.ReachablePositions))
  , ReachablePositionsQueue(std::exchange(o.ReachablePositionsQueue, nullptr))
  , ReachablePositionsValid(o.ReachablePositionsValid)
  , ShortestPathCache(std::move(o.ShortestPathCache))
  , ShortestPathQueue(std::exchange(o.ShortestPathQueue, nullptr))
  , Data(std::move(o.Data))
{
}

Level& Level::operator=(const Level& o)
{
  if (this == &o)
    return *this;

  if (Length != o.Length) {
    delete[] ReachablePositionsQueue;
    delete[] ShortestPathQueue;
    ReachablePositions = LData<bool>(o.Length);
    ReachablePositionsQueue = new LPos[o.Length]();
    ShortestPathCache = LData<uint16_t>(o.Length);
    ShortestPathQueue = new LPos[o.Length]();
  }
  Data = o.Data;
  ReachablePositionsValid = false;

  if (NumberOfCrates != o.NumberOfCrates) {
    delete[] CratePositions;
    CratePositions = new LPos[o.NumberOfCrates];
  }
  std::copy(o.CratePositions, o.CratePositions + o.NumberOfCrates, CratePositions);

  Width = o.Width;
  Height = o.Height;
  Length = o.Length;
  PlayerPosition = o.PlayerPosition;
  NumberOfCrates = o.NumberOfCrates;
  ReachablePositionsValid = o.ReachablePositionsValid;
  return *this;
}

Level& Level::operator=(Level&& o) noexcept
{
  Width = o.Width;
  Height = o.Height;
  Length = o.Length;
  PlayerPosition = o.PlayerPosition;
  NumberOfCrates = o.NumberOfCrates;
  ReachablePositionsValid = o.ReachablePositionsValid;
  std::swap(CratePositions, o.CratePositions);
  std::swap(Data, o.Data);
  std::swap(ReachablePositions, o.ReachablePositions);
  std::swap(ReachablePositionsQueue, o.ReachablePositionsQueue);
  std::swap(ShortestPathCache, o.ShortestPathCache);
  std::swap(ShortestPathQueue, o.ShortestPathQueue);
  return *this;
}

void Level::normalizePlayerPosition()
{
  const LData<bool>& reachableByPlayer = reachablePositions();
  for (const auto& pos : positions()) {
    if (reachableByPlayer[pos]) {
      movePlayerTo(pos);
      return;
    }
  }
  UNREACHABLE();
}

void Level::movePlayerTo(const LPos& position)
{
  ASSERT(position < Length, "position %zu out of range", static_cast<size_t>(position));
  // this shouldn't invalidet the reachable positions.
  ReachablePositionsValid = false;
  Data[PlayerPosition] = static_cast<LElement>(Data[PlayerPosition] ^ LElement::Player);
  Data[position] = static_cast<LElement>(Data[position] | LElement::Player);
  PlayerPosition = position;
}

void Level::moveCrateTo(size_t crateIndex, const LPos& newPosition)
{
  ASSERT(crateIndex < NumberOfCrates, "crateIndex %zu out of range", crateIndex);
  ASSERT(newPosition < Length, "newPos %zu out of range", static_cast<size_t>(newPosition));
  ASSERT((Data[newPosition] & (LElement::Crate | LElement::Wall | LElement::Player)) == 0, "newPos must be free: %zu", static_cast<size_t>(newPosition));
  ReachablePositionsValid = false;
  Data[CratePositions[crateIndex]] = static_cast<LElement>(Data[CratePositions[crateIndex]] ^ LElement::Crate);
  Data[newPosition] = static_cast<LElement>(Data[newPosition] ^ LElement::Crate);
  CratePositions[crateIndex] = static_cast<size_t>(newPosition);
}

void Level::moveCrateFromTo(const LPos& fromPosition, const LPos& toPosition)
{
  ASSERT(fromPosition < Length, "fromPos out of range: %zu", static_cast<size_t>(fromPosition));
  ASSERT(toPosition < Length, "toPos out of range: %zu", static_cast<size_t>(toPosition));
  ASSERT(Data[fromPosition] & LElement::Crate, "fromPos must have a crate on it: %zu", static_cast<size_t>(fromPosition));
  ASSERT((Data[toPosition] & (LElement::Crate | LElement::Wall | LElement::Player)) == 0, "toPos must be free: %zu", static_cast<size_t>(toPosition));
  ReachablePositionsValid = false;
  size_t crateIndex = 0;
  for (; crateIndex < NumberOfCrates; ++crateIndex) {
    if (CratePositions[crateIndex] == fromPosition) {
      break;
    }
  }
  Data[fromPosition] = static_cast<LElement>(Data[fromPosition] ^ LElement::Crate);
  Data[toPosition] = static_cast<LElement>(Data[toPosition] | LElement::Crate);
  CratePositions[crateIndex] = static_cast<size_t>(toPosition);
}

void Level::moveCrateAndPlayerFromTo(const LPos& crateFromPosition, const LPos& crateToPosition, const LPos& playerToPosition)
{
  ASSERT(crateFromPosition < Length, "crateFromPos out of range: %zu", static_cast<size_t>(crateFromPosition));
  ASSERT(crateToPosition < Length, "crateToPos out of range: %zu", static_cast<size_t>(crateToPosition));
  ASSERT(playerToPosition < Length, "playerToPos out of range: %zu", static_cast<size_t>(playerToPosition));
  ASSERT(Data[crateFromPosition] & LElement::Crate, "crateFromPos must have a crate on it: %zu", static_cast<size_t>(crateFromPosition));
  ASSERT((Data[crateToPosition] & LElement::Crate) == 0 || crateFromPosition == crateToPosition, "crateToPos must not have a crate on it: %zu", static_cast<size_t>(crateToPosition));
  ASSERT((Data[playerToPosition] & LElement::Crate) == 0 || playerToPosition == crateFromPosition, "playerToPos must not have a crate on it: %zu", static_cast<size_t>(playerToPosition));
  ASSERT((Data[crateToPosition] & LElement::Wall) == 0, "crateToPos must not have a wall on it: %zu", static_cast<size_t>(crateToPosition));
  ASSERT((Data[playerToPosition] & LElement::Wall) == 0, "playerToPos must not have a wall on it: %zu", static_cast<size_t>(playerToPosition));
  ReachablePositionsValid = false;
  size_t crateIndex = 0;
  for (; crateIndex < NumberOfCrates; ++crateIndex) {
    if (CratePositions[crateIndex] == crateFromPosition) {
      break;
    }
  }
  Data[crateFromPosition] = static_cast<LElement>(Data[crateFromPosition] ^ LElement::Crate);
  Data[crateToPosition] = static_cast<LElement>(Data[crateToPosition] | LElement::Crate);
  CratePositions[crateIndex] = static_cast<size_t>(crateToPosition);
  Data[PlayerPosition] = static_cast<LElement>(Data[PlayerPosition] ^ LElement::Player);
  Data[playerToPosition] = static_cast<LElement>(Data[playerToPosition] | LElement::Player);
  PlayerPosition = playerToPosition;
}

void Level::swapCrateAndPlayer(const LPos& left, const LPos& right)
{
  ASSERT(left < Length, "left position out of range: %zu", static_cast<size_t>(left));
  ASSERT(right < Length, "left position out of range: %zu", static_cast<size_t>(right));
  ASSERT((Data[left] & LElement::Crate) || (Data[right] & LElement::Crate), "left or right must have a crate");
  ASSERT((Data[left] & LElement::Player) || (Data[right] & LElement::Player), "left or right must have a player");
  ReachablePositionsValid = false;
  const LPos& cratePos = (left == PlayerPosition) ? right : left;
  const LPos& playerPos = (left == PlayerPosition) ? left : right;
  size_t crateIndex = 0;
  for (; crateIndex < NumberOfCrates; ++crateIndex) {
    if (CratePositions[crateIndex] == cratePos) {
      break;
    }
  }
  Data[playerPos] = static_cast<LElement>(Data[playerPos] ^ LElement::Player);
  Data[cratePos] = static_cast<LElement>(Data[cratePos] ^ LElement::Crate);
  Data[cratePos] = static_cast<LElement>(Data[cratePos] | LElement::Player);
  Data[playerPos] = static_cast<LElement>(Data[playerPos] | LElement::Crate);
  CratePositions[crateIndex] = static_cast<size_t>(playerPos);
  PlayerPosition = cratePos;
}

void Level::removeCrate(const size_t index)
{
  ASSERT(index < NumberOfCrates, "index out of range: %zu", index);
  ReachablePositionsValid = false;
  Data[CratePositions[index]] = static_cast<LElement>(Data[CratePositions[index]] ^ LElement::Crate);
  if (0 == NumberOfCrates) {
    delete[] CratePositions;
    NumberOfCrates = 0;
    return;
  }
  LPos* newCratePositions = new LPos[NumberOfCrates - 1];
  if (index == 0) {
    std::copy(CratePositions + 1, CratePositions + NumberOfCrates, newCratePositions);
  } else if (index == NumberOfCrates - 1) {
    std::copy(CratePositions, CratePositions + NumberOfCrates - 1, newCratePositions);
  } else {
    std::copy(CratePositions, CratePositions + index, newCratePositions);
    std::copy(CratePositions + index + 1, CratePositions + NumberOfCrates, newCratePositions + index);
  }
  delete[] CratePositions;
  CratePositions = newCratePositions;
  NumberOfCrates -= 1;
}

void Level::addSpot(const LPos& position)
{
  ASSERT(position < Length, "pos out of range: %zu", static_cast<size_t>(position));
  ASSERT((Data[position] & LElement::Wall) == 0, "can not create a spot in the wall: %zu", static_cast<size_t>(position));
  Data[position] = static_cast<LElement>(Data[position] | LElement::Spot);
}

// This is performance critical!
const LData<bool>& Level::reachablePositions()
{
  if (ReachablePositionsValid) {
    return ReachablePositions;
  }
  std::fill(ReachablePositions.begin(), ReachablePositions.end(), false);
  size_t front = 0;
  size_t back = 0;

  ReachablePositionsQueue[back++] = static_cast<size_t>(PlayerPosition);
  ReachablePositions[ReachablePositionsQueue[front]] = true;

  while (front != back) {
    const LPos& p = ReachablePositionsQueue[front++];
    for (auto n : {p - 1, p + 1, p - Width, p + Width}) {
      if (!ReachablePositions[n] && 0 == (Data[n] & (LElement::Crate | LElement::Wall))) {
        ReachablePositions[n] = true;
        ReachablePositionsQueue[back++] = n;
      }
    }
  }
  ReachablePositionsValid = true;
  return ReachablePositions;
}

const LData<bool>& Level::reachablePositionsIgnoringCrates()
{
  ReachablePositionsValid = false;
  std::fill(ReachablePositions.begin(), ReachablePositions.end(), false);
  size_t front = 0;
  size_t back = 0;

  ReachablePositionsQueue[back++] = static_cast<size_t>(PlayerPosition);
  ReachablePositions[ReachablePositionsQueue[front]] = true;

  while (front != back) {
    const LPos& p = ReachablePositionsQueue[front++];
    for (auto n : {p - 1, p + 1, p - Width, p + Width}) {
      if (!ReachablePositions[n] && 0 == (Data[n] & LElement::Wall)) {
        ReachablePositions[n] = true;
        ReachablePositionsQueue[back++] = n;
      }
    }
  }
  return ReachablePositions;
}

State Level::currentState() const
{
  LPos* cratePositions = new LPos[NumberOfCrates];
  std::copy(CratePositions, CratePositions + NumberOfCrates, cratePositions);
  std::sort(cratePositions, cratePositions + NumberOfCrates);
  return State{PlayerPosition, NumberOfCrates, cratePositions};
}

void Level::updateToState(const State& state)
{
  ASSERT(NumberOfCrates == state.numberOfCrates(), "invariant");
  ASSERT(state.playerPosition() < Length, "invariant");

  ReachablePositionsValid = false;
  Data[PlayerPosition] = static_cast<LElement>(Data[PlayerPosition] ^ LElement::Player);
  PlayerPosition = state.playerPosition();
  Data[PlayerPosition] = static_cast<LElement>(Data[PlayerPosition] ^ LElement::Player);

  for (size_t crateNumber = 0; crateNumber < NumberOfCrates; ++crateNumber) {
    Data[CratePositions[crateNumber]] = static_cast<LElement>(Data[CratePositions[crateNumber]] ^ LElement::Crate);
    Data[state.crateAt(crateNumber)] = static_cast<LElement>(Data[state.crateAt(crateNumber)] ^ LElement::Crate);
    CratePositions[crateNumber] = state.crateAt(crateNumber);
  }

  ASSERT([&]() {
    size_t numberOfCrates = 0;
    for (const auto& pos : positions()) {
      numberOfCrates += (Data[pos] & LElement::Crate) != 0;
    }
    ASSERT(numberOfCrates == NumberOfCrates,
           "Check crates after level update, expected %zu, got %zu",
           NumberOfCrates, numberOfCrates);
    return numberOfCrates == NumberOfCrates;
  }(), "Check crates after level update");
}

bool Level::isSolved() const
{
  size_t count = 0;
  for (size_t ncrate = 0; ncrate < NumberOfCrates; ++ncrate) {
    const auto& cratePos = CratePositions[ncrate];
    count += (Data[cratePos] & LElement::Spot) != 0;
  }
  ASSERT(count <= NumberOfCrates, "invariant");
  return count == NumberOfCrates;
}

std::optional<uint16_t> Level::shortestPlayerPathTo(const LPos& position)
{
  if (position == PlayerPosition) {
    return std::make_optional(0u);
  }
  std::fill(ShortestPathCache.begin(), ShortestPathCache.end(), UINT16_MAX);
  std::size_t front = 0;
  std::size_t back = 0;
  ShortestPathCache[PlayerPosition] = 0;
  ShortestPathQueue[back++] = PlayerPosition;
  while (front != back) {
    const auto& pos = ShortestPathQueue[front++];
    for (const auto& next : {pos - 1, pos + 1, pos - Width, pos + Width}) {
      if (0 == (Data[next] & (LElement::Crate | LElement::Wall))) {
        if (ShortestPathCache[pos] + 1 < ShortestPathCache[next]) {
          ShortestPathCache[next] = ShortestPathCache[pos] + 1;
          ShortestPathQueue[back++] = next;
        }
      }
    }
  }
  if (UINT16_MAX == ShortestPathCache[position]) {
    return std::nullopt;
  }
  return std::make_optional(ShortestPathCache[position] + 1);
}

void Level::visualize() const
{
  for (const auto& pos : positions()) {
    if (pos && static_cast<size_t>(pos) % Width == 0) {
      puts("");
    }
    if (isWallAt(pos)) {
      putchar(Level::WALL_CHAR);
    } else if (isPlayerAt(pos)) {
      if (isSpotAt(pos)) {
        putchar(Level::PLAYER_ON_SPOT_CHAR);
      } else {
        putchar(Level::PLAYER_CHAR);
      }
    } else if (isCrateAt(pos)) {
      if (isSpotAt(pos)) {
        putchar(Level::CRATE_ON_SPOT_CHAR);
      } else {
        putchar(Level::CRATE_CHAR);
      }
    } else if (isSpotAt(pos)) {
      putchar(Level::SPOT_CHAR);
    } else {
      putchar(Level::FLOOR_CHAR);
    }
  }
  puts("");
}


Level Level::parseFromString(const std::string& s)
{
  std::vector<std::string> lines;
  std::string::size_type last_pos = 0;
  std::string::size_type pos = 0;
  while ((pos = s.find('\n', pos)) != std::string::npos) {
    lines.push_back(s.substr(last_pos, pos - last_pos));
    last_pos = pos + 1;
    pos = pos + 1;
  }
  if (last_pos < s.length()) {
    lines.push_back(s.substr(last_pos));
  }
  return parseFromLines(lines);
}

Level Level::parseFromLines(const std::vector<std::string>& lines)
{
  size_t cols = 0;
  for (auto row : lines) {
    cols = std::max(cols, row.length());
  }
  size_t idx = 0;
  LElement* level = new LElement[cols * lines.size()];
  for (auto row : lines) {
    for (auto c : row) {
      LElement e;
      switch (c) {
      case WALL_CHAR:
        e = LElement::Wall;
        break;
      case CRATE_CHAR:
        e = static_cast<LElement>(LElement::Crate | LElement::Floor);
        break;
      case SPOT_CHAR:
        e = static_cast<LElement>(LElement::Spot | LElement::Floor);
        break;
      case CRATE_ON_SPOT_CHAR:
        e = static_cast<LElement>(LElement::Spot | LElement::Crate | LElement::Floor);
        break;
      case PLAYER_CHAR:
        e = static_cast<LElement>(LElement::Player | LElement::Floor);
        break;
      case PLAYER_ON_SPOT_CHAR:
        e = static_cast<LElement>(LElement::Spot | LElement::Player | LElement::Floor);
        break;
      case FLOOR_CHAR:
      case '-':
      case '_':
        e = LElement::Floor;
        break;
      default:
        // TODO: report error
        e = LElement::Floor;
        break;
      }
      level[idx++] = e;
    }
    for (size_t i = row.length(); i < cols; ++i) {
      level[idx++] = LElement::Floor;
    }
  }
  return Level(cols, lines.size(), level);
}
 
LevelValidationResult Level::verifyAndNormalizeLevel()
{
  if (0 == Width || 0 == Height || 0 == Length || 10000 < Width || 10000 < Height) {
    // TODO: review / document the maximum width/height
    return LevelValidationResult::bad("Invalid dimensions");
  }
  if (Width * Height != Length) {
    return LevelValidationResult::bad("Invalid level length");
  }
  {
    // Check there is exactly one Player
    int playerCounter = 0;
    for (const auto& pos : positions()) {
      if (isPlayerAt(pos)) {
        ++playerCounter;
      }
    }
    if (0 == playerCounter) {
      return LevelValidationResult::bad("No player found");
    }
    if (1 < playerCounter) {
      return LevelValidationResult::bad("More than one player found");
    }
  }
  {
    // Find all reachable positions. This is implemented here again, because
    // it needs to make more checks, since the level might be invalid.
    LData<bool> reachable(Length);
    LPos* queue = new LPos[Length]();
    size_t front = 0;
    size_t back = 0;
    queue[back++] = PlayerPosition;
    reachable[queue[front]] = true;
    while (front != back) {
      const LPos& p = queue[front++];
      for (const LPos& n  : {(static_cast<size_t>(p) % Width == 0) ? p : p - 1,
                             (static_cast<size_t>(p) % Width == Width - 1) ? p : p + 1,
                             (static_cast<size_t>(p) / Width == 0) ? p : p - Width,
                             (static_cast<size_t>(p) / Width == Height - 1) ? p : p + Width}) {
        if (n != p && !reachable[n] && !isWallAt(n)) {
          reachable[n] = true;
          queue[back++] = n;
        }
      }
    }
    delete[] queue;
    queue = nullptr;
    // First + Last row and First + Last column must be unreachable
    for (size_t col = 0; col < Width; ++col) {
      if (reachable[LPos(col, Length)] || reachable[LPos(Length - col - 1, Length)]) {
        return LevelValidationResult::bad("Level is not enclosed with walls");
      }
    }
    for (size_t row = 0; row < Height; ++row) {
      if (reachable[LPos(row * Width, Length)] || reachable[LPos((row + 1) * Width - 1, Length)]) {
        return LevelValidationResult::bad("Level is not enclosed with walls");
      }
    }
    // every final storage location must be reachable
    for (const auto& pos : positions()) {
      if ((Data[pos] & LElement::Spot) && !reachable[pos]) {
        return LevelValidationResult::bad("Final storage location not reachable by player");
      }
    }
    // every crate tile and one of its sides must be reachable
    for (const auto& pos : positions()) {
      if ((Data[pos] & LElement::Crate)) {
        if (!reachable[pos]) {
          return LevelValidationResult::bad("Crate location not reachable by player");
        }
        if (!reachable[pos - 1] &&
            !reachable[pos + 1] &&
            !reachable[pos + Width] &&
            !reachable[pos - Width]) {
          return LevelValidationResult::bad("Crate can never be pushed by player");
        }
      }
    }
    // every unreachable position must either be a floor or a wall
    // for easier algorithms, make every onreachable location a wall
    for (const auto& pos : positions()) {
      if (!reachable[pos]) {
        switch (Data[pos]) {
        case Wall:
          break;
        case Floor:
          break;
        case Crate:
          return LevelValidationResult::bad("Crate position not reachable");
        case Spot:
          return LevelValidationResult::bad("Final storage location not reachable");
        case Player:
          return LevelValidationResult::bad("Player location not reachable");
        }
        Data[pos] = LElement::Wall;
      }
    }
  }
  {
    // Number of final storage locations must be >= to the number of crates
    int crateCounter = 0;
    int spotCounter = 0;
    for (const auto& pos : positions()) {
      if (isCrateAt(pos)) {
        ++crateCounter;
      }
      if (isSpotAt(pos)) {
        ++spotCounter;
      }
    }
    if (0 == crateCounter) {
      return LevelValidationResult::bad("No crates found");
    }
    if (0 == spotCounter) {
      return LevelValidationResult::bad("No final storage locations found");
    }
    if (crateCounter > spotCounter) {
      return LevelValidationResult::bad("Not enough final storage locations found for all the crates");
    }
  }
  return LevelValidationResult::good();
}
