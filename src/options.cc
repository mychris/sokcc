/* SPDX-License-Identifier: MIT */
#include "options.h"
#include <cstdlib>
#include <cstring>

#ifndef DEFAULT_PROGRAM_NAME
#define DEFAULT_PROGRAM_NAME "sokcc"
#endif

Opts Options
{
};

std::string ProgramName{};

void parseOptionsFromCommandLine(int iargc, char **argv)
{
  if (iargc < 0) {
    fprintf(stderr, "%s: invalid argc: %d\n", DEFAULT_PROGRAM_NAME, iargc);
    exit(1);
  }

  size_t argc = static_cast<size_t>(iargc);
  size_t pos = 0;
  if (0 == argc) {
    ProgramName = DEFAULT_PROGRAM_NAME;
  } else {
    std::string tmp(argv[0]);
    ProgramName.reserve(tmp.size());
    ProgramName.insert(0, std::move(std::string(argv[0])));
    ++pos;
  }
  ProgramName.shrink_to_fit();

  while (pos < argc) {
    if (strcmp("--", argv[pos])) {
      break;
    }
    ++pos;
  }
}
