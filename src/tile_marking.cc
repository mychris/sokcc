/* SPDX-License-Identifier: MIT */
#include "tile_marking.h"
#include "debug.h"

std::size_t TileMarkings::numberOfMarkedTiles() const noexcept
{
  std::size_t result = 0;
  for (const auto& pos : LPosRange(LPos(0, Length), Length)) {
    if (Data[pos]) {
      result += 1;
    }
  }
  return result;
}

void TileMarkings::merge(const TileMarkings& o)
{
  ASSERT(Length == o.Length, "invariant");
  for (const auto& pos : LPosRange(LPos(0, Length), Length)) {
    Data[pos] |= o.Data[pos];
  }
}

TileMarkings TileMarkings::join(const TileMarkings& o) const
{
  TileMarkings result(*this);
  result.merge(o);
  return result;
}
