/* SPDX-License-Identifier: MIT */
#include "carray.h"
#include "debug.h"
#include "lurd.h"
#include <queue>
#include <optional>
#include <string>
#include <vector>

LURD::size_type LURD::numberOfMoves() const
{
  return Actions.size();
}

LURD::size_type LURD::numberOfPushes() const
{
  LURD::size_type result = 0;
  for (const auto& a : *this) {
    if (a == Action::PushLeft || a == Action::PushUp || a == Action::PushRight || a == Action::PushDown) {
      result += 1;
    }
  }
  return result;
}

void LURD::visualizeLevel(const Level& level) const
{
  LData<bool> walls(level.length());
  LData<bool> crates(level.length());
  LData<bool> spots(level.length());
  LPos player = level.playerPosition();
  for (const auto& pos : level.positions()) {
    if (level.isWallAt(pos)) {
      walls[pos] = true;
    }
    if (level.isPlayerAt(pos)) {
      player = pos;
    }
    if (level.isCrateAt(pos)) {
      crates[pos] = true;
    }
    if (level.isSpotAt(pos)) {
      spots[pos] = true;
    }
  }
  for (const auto& pos : level.positions()) {
    if (pos && static_cast<size_t>(pos) % level.width() == 0) putchar('\n');
    if (walls[pos]) putchar(Level::WALL_CHAR);
    else if (pos == player && spots[pos]) putchar(Level::PLAYER_CHAR);
    else if (pos == player) putchar(Level::PLAYER_CHAR);
    else if (crates[pos] && spots[pos]) putchar(Level::CRATE_ON_SPOT_CHAR);
    else if (crates[pos]) putchar(Level::CRATE_CHAR);
    else if (spots[pos]) putchar(Level::SPOT_CHAR);
    else putchar(Level::FLOOR_CHAR);
  }
  putchar('\n');
  for (const auto action : Actions) {
    switch (action) {
    case Action::MoveLeft:
      player -= 1;
      break;
    case Action::MoveRight:
      player += 1;
      break;
    case Action::MoveDown:
      player += level.width();
      break;
    case Action::MoveUp:
      player -= level.width();
      break;
    case Action::PushLeft:
      crates[player - 1] = false;
      crates[player - 2] = true;
      player -= 1;
      break;
    case Action::PushRight:
      crates[player + 1] = false;
      crates[player + 2] = true;
      player += 1;
      break;
    case Action::PushDown:
      crates[player + level.width()] = false;
      crates[player + level.width() * 2] = true;
      player += level.width();
      break;
    case Action::PushUp:
      crates[player - level.width()] = false;
      crates[player - level.width() * 2] = true;
      player -= level.width();
      break;
    default:
      UNREACHABLE();
    }
    for (const auto& pos : level.positions()) {
      if (pos && static_cast<size_t>(pos) % level.width() == 0) putchar('\n');
      if (walls[pos]) putchar(Level::WALL_CHAR);
      else if (pos == player && spots[pos]) putchar(Level::PLAYER_CHAR);
      else if (pos == player) putchar(Level::PLAYER_CHAR);
      else if (crates[pos] && spots[pos]) putchar(Level::CRATE_ON_SPOT_CHAR);
      else if (crates[pos]) putchar(Level::CRATE_CHAR);
      else if (spots[pos]) putchar(Level::SPOT_CHAR);
      else putchar(Level::FLOOR_CHAR);
    }
    putchar('\n');
  }
}

LURD LURD::fromLURDString(const std::string &s)
{
  std::vector<Action> actions;
  for (const char c : s) {
    switch (c) {
    case MOVE_LEFT_CHAR:
      actions.push_back(Action::MoveLeft);
      break;
    case MOVE_UP_CHAR:
      actions.push_back(Action::MoveUp);
      break;
    case MOVE_RIGHT_CHAR:
      actions.push_back(Action::MoveRight);
      break;
    case MOVE_DOWN_CHAR:
      actions.push_back(Action::MoveDown);
      break;
    case PUSH_LEFT_CHAR:
      actions.push_back(Action::PushLeft);
      break;
    case PUSH_UP_CHAR:
      actions.push_back(Action::PushUp);
      break;
    case PUSH_RIGHT_CHAR:
      actions.push_back(Action::PushRight);
      break;
    case PUSH_DOWN_CHAR:
      actions.push_back(Action::PushDown);
      break;
    default:
      return LURD();
    }
  }
  return LURD(actions);
}

LURD LURD::fromStates(const Level& level, const std::vector<State>& states)
{
  if (states.size() < 2) {
    return LURD();
  }
  std::vector<Action> actions;
  LPos playerPosition = level.playerPosition();
  for (size_t fromPos = 0, toPos = 1; toPos < states.size(); ++fromPos, ++toPos) {
    auto transition = findCrateThatMoved(states[fromPos], states[toPos]);
    GUARANTEE(transition.has_value(), "Invalid transition");
    auto player = findPlayerPositionsForTransition(level, transition.value());
    GUARANTEE(player.has_value(), "Invalid transition");
    auto pushAction = calculateActionForTransition(level, transition.value());
    auto shortestPath = findShortestPlayerPath(level, states[fromPos], playerPosition, player.value().first);
    auto moveActions = pathToActions(level, shortestPath);
    actions.insert(actions.end(), moveActions.begin(), moveActions.end());
    actions.push_back(pushAction);
    playerPosition = player.value().second;
  }
  return LURD(actions);
}

std::optional<std::pair<LPos, LPos>> LURD::findCrateThatMoved(const State& left, const State& right)
{
  ASSERT(left.numberOfCrates() == right.numberOfCrates(), "invariant");
  std::optional<LPos> leftCrateMoved = std::nullopt;
  std::optional<LPos> rightCrateMoved = std::nullopt;
  for (std::size_t i = 0; i < left.numberOfCrates(); ++i) {
    const auto& leftPos = left[i];
    const auto& rightPos = right[i];
    if (!right.doesCrateOccupy(leftPos)) {
      if (leftCrateMoved.has_value()) {
        ASSERT(false, "More than one crate changed its position!");
        return std::nullopt;
      }
      leftCrateMoved = leftPos;
    }
    if (!left.doesCrateOccupy(rightPos)) {
      if (rightCrateMoved.has_value()) {
        ASSERT(false, "More than one crate changed its position!");
        return std::nullopt;
      }
      rightCrateMoved = rightPos;
    }
  }
  if (!leftCrateMoved.has_value() || !rightCrateMoved.has_value()) {
    return std::nullopt;
  }
  return std::make_optional<std::pair<LPos, LPos>>({leftCrateMoved.value(), rightCrateMoved.value()});
}

std::optional<std::pair<LPos, LPos>> LURD::findPlayerPositionsForTransition(const Level& level, const std::pair<LPos, LPos>& transition)
{
  const auto& leftCrateMoved = transition.first;
  const auto& rightCrateMoved = transition.second;
  if (rightCrateMoved == leftCrateMoved - 1) {
    return {{leftCrateMoved + 1, rightCrateMoved + 1}};
  } else if (rightCrateMoved == leftCrateMoved + 1) {
    return {{leftCrateMoved - 1, rightCrateMoved - 1}};
  } else if (rightCrateMoved == leftCrateMoved + level.width()) {
    return {{leftCrateMoved - level.width(), rightCrateMoved - level.width()}};
  } else if (rightCrateMoved == leftCrateMoved - level.width()) {
    return {{leftCrateMoved + level.width(), rightCrateMoved + level.width()}};
  } else {
    ASSERT(false, "Crate moved more than one tile!");
    return std::nullopt;
  }
}

LURD::Action LURD::calculateActionForTransition(const Level& level, const std::pair<LPos, LPos>& transition)
{
  const auto& leftCrateMoved = transition.first;
  const auto& rightCrateMoved = transition.second;
  if (rightCrateMoved == leftCrateMoved - 1) {
    return Action::PushLeft;
  } else if (rightCrateMoved == leftCrateMoved + 1) {
    return Action::PushRight;
  } else if (rightCrateMoved == leftCrateMoved + level.width()) {
    return Action::PushDown;
  } else if (rightCrateMoved == leftCrateMoved - level.width()) {
    return Action::PushUp;
  } else {
    UNREACHABLE();
  }
}

std::vector<LPos> LURD::findShortestPlayerPath(const Level& level, const State& state, const LPos& from, const LPos& to)
{
  LData<std::size_t> shortCache(level.length());
  std::fill(shortCache.begin(), shortCache.end(), SIZE_MAX);
  std::vector<LPos> prevCache(level.length(), from);
  std::queue<LPos> queue;
  if (from == to) {
    return {};
  }
  const size_t width = level.width();
  shortCache[from] = 0;
  queue.push(from);
  while (!queue.empty()) {
    const auto& pos = queue.front();
    for (const auto& next : {pos - 1, pos + 1, pos - width, pos + width}) {
      if (!level.isWallAt(next) && !state.doesCrateOccupy(next)) {
        if (shortCache[pos] + 1 < shortCache[next]) {
          shortCache[next] = shortCache[pos] + 1;
          prevCache[static_cast<size_t>(next)] = pos;
          queue.push(next);
        }
      }
    }
    queue.pop();
  }

  std::vector<LPos> result{};
  LPos pos(to);
  while (from != pos) {
    result.push_back(pos);
    pos = prevCache[static_cast<size_t>(pos)];
  }
  result.push_back(from);
  result.shrink_to_fit();
  std::reverse(result.begin(), result.end());
  return result;
}

std::vector<LURD::Action> LURD::pathToActions(const Level& level, const std::vector<LPos>& path)
{
  std::vector<LURD::Action> result;
  if (path.size() < 2) {
    return result;
  }
  auto fromIt = path.begin();
  auto toIt = ++path.begin();
  while (toIt != path.end()) {
    const auto& from = *fromIt;
    const auto& to = *toIt;
    if (to == from - 1) {
      result.push_back(Action::MoveLeft);
    } else if (to == from + 1) {
      result.push_back(Action::MoveRight);
    } else if (to == from + level.width()) {
      result.push_back(Action::MoveDown);
    } else if (to == from - level.width()) {
      result.push_back(Action::MoveUp);
    } else {
      ASSERT(false, "invalid path!");
      return std::vector<LURD::Action>();
    }
    ++fromIt;
    ++toIt;
  }
  return result;
}
