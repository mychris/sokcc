/* SPDX-License-Identifier: MIT */
#include "debug.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void
assertion_error(const char* file, int line, const char* function, const char* message)
{
  assertion_error(file, line, function, message, "");
}

void
assertion_error(const char* file, int line, const char* function, const char* message,
                const char* details, ...)
{
  va_list detail_args;
  va_start(detail_args, details);
  fprintf(stderr, "%s: %s:%d: ", program_invocation_name, file, line);
  if (function && function[0] != '\0') {
    fprintf(stderr, "%s: ", function);
  }
  fprintf(stderr, "%s", message);
  if (details && details[0] != '\0') {
    fprintf(stderr, ": ");
    vfprintf(stderr, details, detail_args);
  }
  va_end(detail_args);
  fprintf(stderr, "\n");
  abort();
}
