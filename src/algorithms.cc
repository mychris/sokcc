/* SPDX-License-Identifier: MIT */
#include "algorithms.h"
#include <alloca.h>
#include <cstring>

void reachablePositions(const size_t width,
                        const size_t height,
                        const LPos& position,
                        const std::function<bool(const LPos&)> &predicate,
                        LData<bool>& result)
{
  const size_t length = width * height;
  std::fill(result.begin(), result.end(), false);
  LPos* queue = new LPos[length];
  size_t front = 0;
  size_t back = 0;

  queue[back++] = position;
  result[queue[front]] = true;

  while (front != back) {
    const LPos p = queue[front++];
    
    for (const LPos n : {p - 1, p + 1, p - width, p + width}) {
      if (!result[n] && predicate(n)) {
        result[n] = true;
        queue[back++] = n;
      }
    }
  }
  delete[] queue;
}
