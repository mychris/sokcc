/* SPDX-License-Identifier: MIT */
#include "dead_spot.h"
#include "level.h"
#include "options.h"
#include "solver.h"
#include <climits>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>

bool isCommentLine(const std::string &line)
{
  return line.empty() || (line[0] != ' ' && line[0] != '#' && line[0] != '-' && line[0] != '_');
}

std::vector<std::string> splitLines(const std::string &s)
{
  std::vector<std::string> lines;
  std::string::size_type last_pos = 0;
  std::string::size_type pos = 0;
  while ((pos = s.find('\n', pos)) != std::string::npos) {
    std::string line = s.substr(last_pos, pos - last_pos);
    if (isCommentLine(line)) {
      lines.push_back("");
    } else {
      lines.push_back(line);
    }
    last_pos = pos + 1;
    pos = pos + 1;
  }
  if (last_pos < s.length()) {
    std::string line = s.substr(last_pos);
    if (isCommentLine(line)) {
      lines.push_back("");
    } else {
      lines.push_back(line);
    }
  }
  return lines;
}

std::string readFileToString(std::ifstream &in)
{
  std::ifstream::pos_type fileSize = in.tellg();
  in.seekg(0, std::ios::beg);

  std::vector<char> bytes(static_cast<std::vector<char>::size_type>(fileSize));
  in.read(bytes.data(), fileSize);

  return std::string(bytes.data(), static_cast<std::string::size_type>(fileSize));
}

Level readLevelFrom(const std::string &path, int level_number)
{
  std::ifstream ifs(path.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
  if (!ifs.is_open()) {
    fprintf(stderr, "%s: %s: can not read level\n", ProgramName.c_str(), path.c_str());
    exit(1);
  }
  std::string content = readFileToString(ifs);
  std::vector<std::string> lines = splitLines(content);
  std::vector<std::string>::size_type idx = 0;
  for (; idx < lines.size() && lines[idx].empty(); idx++)
    ;
  int current_level = 1;
  while (current_level < level_number) {
    for (; idx < lines.size() && !lines[idx].empty(); idx++)
      ;
    for (; idx < lines.size() && lines[idx].empty(); idx++)
      ;
    current_level++;
  }
  if (idx == lines.size()) {
    fprintf(stderr, "%s: %d: level number too high\n", ProgramName.c_str(), level_number);
    exit(1);
  }
  std::vector<std::string> levelLines;
  for (; idx < lines.size() && !lines[idx].empty(); idx++) {
    levelLines.push_back(lines[idx]);
  }
  return Level::parseFromLines(levelLines);
}

int main(int argc, char *argv[])
{
  parseOptionsFromCommandLine(argc, argv);
  if (argc != 2 && argc != 3) {
    fprintf(stderr, "usage: %s file [level-number]\n", argv[0]);
    exit(1);
  }
  int level_number = 1;
  if (argc == 3) {
    char *endptr = nullptr;
    long result = strtol(argv[2], &endptr, 10);
    if (((result == LONG_MIN || result == LONG_MAX) && errno == ERANGE)
        || *endptr != '\0'
        || result <= 0
        || result > INT_MAX) {
      fprintf(stderr, "%s: %s: invalid level-number\n", argv[0], argv[2]);
      exit(1);
    }
    level_number = static_cast<int>(result);
  }
  Level lvl = readLevelFrom(std::string(argv[1]), level_number);
  LevelValidationResult levelValidation = lvl.verifyAndNormalizeLevel();
  if (!levelValidation.isGood()) {
    fprintf(stderr, "%s: given level is not valid: %s\n", ProgramName.c_str(), levelValidation.getMessage().c_str());;
    return 1;
  }
  AsyncSolver worker(Solver::Builder()
                     .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
                     .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
                     .build<SimpleBreadthFirstSolver>());
  worker.solveLevelAsync(lvl);
  LURD solution = worker.computedSoltion();
  solution.visualizeLevel(lvl);
  printf("%s\n", solution.toString().c_str());
  printf("Moves %zu\nPushes %zu\n", solution.numberOfMoves(), solution.numberOfPushes());
  return 0;
}
