/* SPDX-License-Identifier: MIT */
#include "algorithms.h"
#include "dead_spot.h"
#include "level.h"
#include "state_table.h"
#include "solver.h"
#include "tile_marking.h"
#include <cstdint>
#include <vector>
#include <thread>
#include <chrono>

void AsyncSolver::solveLevelAsync(const Level& level)
{
  if (LastFuture.valid()) {
    LastFuture.wait();
  }
  LastFuture = std::async(std::launch::async, [=] {
    return RealSolver->solveLevel(level);
  });
}

bool AsyncSolver::solutionComputed() const
{
  if (!LastFuture.valid()) {
    return false;
  }
  return LastFuture.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

LURD AsyncSolver::computedSoltion()
{
  GUARANTEE(LastFuture.valid(), "invariant");
  return LastFuture.get();
}

SimpleBreadthFirstSolver::SimpleBreadthFirstSolver(std::unique_ptr<StaticDeadTileChecker> sdtc,
                                                   std::unique_ptr<DynamicDeadTileChecker> ddtc)
  : OriginalLvl()
  , Table(nullptr)
  , StaticDeadSpotChecker(std::move(sdtc))
  , DynamicDeadSpotChecker(std::move(ddtc))
  , Queue()
  , BestSolutionMoves(SIZE_MAX)
  , BestSolutionPushes(SIZE_MAX)
{
}

SimpleBreadthFirstSolver::~SimpleBreadthFirstSolver()
{
}

LURD SimpleBreadthFirstSolver::solveLevel(const Level& givenLevel)
{
  {
    // setup for this run
    Table = std::make_unique<StateTable>(givenLevel);
  }
  OriginalLvl = givenLevel;
  Level level(OriginalLvl);
  StaticDeadSpotChecker->configureFor(OriginalLvl);
  DynamicDeadSpotChecker->configureFor(OriginalLvl);
  StateTableEntry* bestSolution;
  BestSolutionMoves = SIZE_MAX;
  BestSolutionPushes = SIZE_MAX;
  //level.normalizePlayerPosition();
  Queue.push(Table->insert(level.currentState()));
  Queue.front()->setPushes(0);
  Queue.front()->setMoves(0);
  while (!Queue.empty()) {
    StateTableEntry* thisEntry = Queue.front();
    Queue.pop();
    level.updateToState(thisEntry->entry());
    if (level.isSolved()) {
      if (thisEntry->numberOfPushes() < BestSolutionPushes ||
          (thisEntry->numberOfPushes() == BestSolutionPushes && thisEntry->numberOfMoves() < BestSolutionMoves)) {
        BestSolutionPushes = thisEntry->numberOfPushes();
        BestSolutionMoves = thisEntry->numberOfMoves();
        bestSolution = thisEntry;
      }
    }
    if (thisEntry->numberOfPushes() < BestSolutionPushes ||
        (thisEntry->numberOfPushes() == BestSolutionPushes && thisEntry->numberOfMoves() < BestSolutionMoves)) {
      handleState(level, thisEntry);
    }
  }
  LURD solution = (bestSolution) ? constructLurdFromEndState(bestSolution->entry()) : LURD::fromLURDString("");
  {
    // reset for the next run
    Queue = std::queue<StateTableEntry*>();
    Table.reset(nullptr);
  }
  return solution;
}

LURD SimpleBreadthFirstSolver::constructLurdFromEndState(const State& end)
{
  // calulate the size
  std::size_t requiredSize = 0;
  for (const StateTableEntry *entry = Table->entry(end);
       entry;
       entry = entry->predecessor()) {
    ++requiredSize;
  }
  std::vector<State> path(requiredSize, OriginalLvl.currentState());
  std::size_t pos = requiredSize;
  for (const StateTableEntry *entry = Table->entry(end);
       entry;
       entry = entry->predecessor()) {
    path[pos - 1] = entry->entry();
    --pos;
  }
  ASSERT(pos == 0, "would be strange if not");
  // need to store again. The state table contains the starting state
  // with a normalized player position.
  path[0] = OriginalLvl.currentState();
  return LURD::fromStates(OriginalLvl, path);
}

void SimpleBreadthFirstSolver::handleState(Level& level, StateTableEntry* thisEntry)
{
  const LPos originalPlayerPosition = level.playerPosition();
  const size_t width = level.width();
  const size_t numberOfCrates = level.numberOfCrates();
  for (size_t ncrate = 0; ncrate < numberOfCrates; ++ncrate) {
    const LPos cratePos = level.crateAt(ncrate);
    for (const auto& newCratePos : {cratePos - 1, cratePos + 1, cratePos - width, cratePos + width}) {
      const LPos playerPushPos = cratePos + cratePos.difference(newCratePos);
      if (!level.isCrateAt(newCratePos) &&
          !StaticDeadSpotChecker->isCrateOnDeadTile(level, newCratePos)) {
        const std::optional<uint16_t> playerMovesToPushPos = level.shortestPlayerPathTo(playerPushPos);
        if (playerMovesToPushPos.has_value()) {
          if (level.isPlayerAt(newCratePos)) {
            level.swapCrateAndPlayer(level.playerPosition(), cratePos);
          } else {
            level.moveCrateTo(ncrate, newCratePos);
          }
          level.movePlayerTo(cratePos);
          if (!DynamicDeadSpotChecker->isCrateOnDeadTile(level, newCratePos)) {
            const uint16_t playerMoves = *playerMovesToPushPos + 1;
            // level.normalizePlayerPosition();
            bool newEntry = false;
            StateTableEntry *nextStateEntry = Table->getOrInsertEntry(level.currentState(), newEntry);
            if (newEntry) {
              nextStateEntry->setPushes(thisEntry->numberOfPushes() + 1);
              nextStateEntry->setMoves(thisEntry->numberOfMoves() + playerMoves);
              Queue.push(nextStateEntry);
              nextStateEntry->setPredecessor(thisEntry);
            } else if (nextStateEntry->numberOfPushes() >
                       thisEntry->numberOfPushes() + 1) {
              nextStateEntry->setPushes(thisEntry->numberOfPushes() + 1);
              nextStateEntry->setMoves(thisEntry->numberOfMoves() + playerMoves);
              Queue.push(nextStateEntry);
              nextStateEntry->setPredecessor(thisEntry);
            } else if (nextStateEntry->numberOfPushes() ==
                           thisEntry->numberOfPushes() + 1 &&
                       nextStateEntry->numberOfMoves() >
                           thisEntry->numberOfMoves() + playerMoves) {
              nextStateEntry->setPushes(thisEntry->numberOfPushes() + 1);
              nextStateEntry->setMoves(thisEntry->numberOfMoves() + playerMoves);
              Queue.push(nextStateEntry);
              nextStateEntry->setPredecessor(thisEntry);
            }
          }
          level.moveCrateAndPlayerFromTo(newCratePos, cratePos, originalPlayerPosition);
        }
      }
    }
  }
}

void SimpleBreadthFirstSolver::visualize(const Level& level) const
{
  for (const auto& pos : level.positions()) {
    if (pos && static_cast<size_t>(pos) % level.width() == 0) {
      puts("");
    }
    if (level.isWallAt(pos)) {
      putchar(Level::WALL_CHAR);
    } else if (level.isPlayerAt(pos)) {
      if (level.isSpotAt(pos)) {
        putchar(Level::PLAYER_ON_SPOT_CHAR);
      } else {
        putchar(Level::PLAYER_CHAR);
      }
    } else if (level.isCrateAt(pos)) {
      if (level.isSpotAt(pos)) {
        putchar(Level::CRATE_ON_SPOT_CHAR);
      } else {
        putchar(Level::CRATE_CHAR);
      }
    } else if (level.isSpotAt(pos)) {
      putchar(Level::SPOT_CHAR);
    } else {
      putchar(Level::FLOOR_CHAR);
    }
  }
  puts("");
}

void SimpleBreadthFirstSolver::visualize(const Level& level, const TileMarkings& markings) const
{
  for (const auto& pos : level.positions()) {
    if (pos && static_cast<size_t>(pos) % level.width() == 0) {
      puts("");
    }
    if (level.isWallAt(pos)) {
      putchar(Level::WALL_CHAR);
    } else if (markings.isTileMarked(pos)) {
      putchar('.');
    } else {
      putchar(Level::FLOOR_CHAR);
    }
  }
  puts("");
}
