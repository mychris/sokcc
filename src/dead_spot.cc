/* SPDX-License-Identifier: MIT */
#include "algorithms.h"
#include "carray.h"
#include "debug.h"
#include "dead_spot.h"
#include <algorithm>
#include <utility>
#include <vector>
#include <queue>

void StaticDeadTileNoOpCheckingAlgorithm::markDeadTiles(const Level &level, TileMarkings& result)
{
  LData<bool> reachable(level.length());
  reachablePositions(level.width(),
                     level.height(),
                     level.playerPosition(),
                     [&](const LPos& pos) { return !level.isWallAt(pos); },
                     reachable);

  for (const auto& pos : level.positions()) {
    if (!reachable[pos]) {
      result.markTile(pos);
    }
  }
}

void StaticDeadTileCheckCornersCheckingAlgorithm::markDeadTiles(const Level &level, TileMarkings& result)
{
  LData<bool> reachable(level.length());
  reachablePositions(level.width(),
                     level.height(),
                     level.playerPosition(),
                     [&](const LPos& pos) { return !level.isWallAt(pos); },
                     reachable);
  for (const auto& pos : level.positions()) {
    if (!reachable[pos]) {
      result.markTile(pos);
    }
  }

  const auto width = level.width();
  for (size_t row = 1; row < level.height() - 1; ++row) {
    for (size_t col = 1; col < width - 1; ++col) {
      const LPos pos = LPos(row * width + col, level.length());
      if (!level.isSpotAt(pos) && !level.isWallAt(pos)) {
        if ((level.isWallAt(pos - 1) && level.isWallAt(pos - width))
            || (level.isWallAt(pos + 1) && level.isWallAt(pos - width))
            || (level.isWallAt(pos + 1) && level.isWallAt(pos + width))
            || (level.isWallAt(pos - 1) && level.isWallAt(pos + width))) {
          result.markTile(pos);
        }
      }
    }
  }
}

void StaticDeadTileSinglePushCheckingAlgorithm::markDeadTiles(const Level &level, TileMarkings& result)
{
  const auto width = level.width();
  const auto height = level.height();
  LData<bool> marked(level.length());
  std::fill(marked.begin(), marked.end(), false);
  // start with all the spots, we know those are not dead, since they are the targets
  for (const auto& pos : level.positions()) {
    if (level.isSpotAt(pos)) {
      marked[pos] = true;
    }
  }

  // continue searching until a full round didn't give any new information
  bool found_something = true;
  while (found_something) {
    found_something = false;
    for (const auto& pos : level.positions()) {
      if (marked[pos] || result.isTileMarked(pos) || level.isWallAt(pos)) {
        continue;
      }
      // for every position which has not been marked
      // suppose a crate stands on that position, is it possible to push it onto a tile,
      // which has already been marked
      if (pos > width && pos < width * height - width) {
        // try to push up
        if (marked[pos - width] && !result.isTileMarked(pos - width) && !level.isWallAt(pos + width)) {
          found_something = true;
          marked[pos] = true;
        }
        // try to push down
        if (marked[pos + width] && !result.isTileMarked(pos + width) && !level.isWallAt(pos - width)) {
          found_something = true;
          marked[pos] = true;
        }
      }
      if (static_cast<size_t>(pos) % width != 0 && static_cast<size_t>(pos) % width != width - 1) {
        // try to push left
        if (marked[pos - 1] && !result.isTileMarked(pos - 1) && !level.isWallAt(pos + 1)) {
          found_something = true;
          marked[pos] = true;
        }
        // try to push right
        if (marked[pos + 1] && !result.isTileMarked(pos + 1) && !level.isWallAt(pos - 1)) {
          found_something = true;
          marked[pos] = true;
        }
      }
    }
  }

  for (const auto& pos : level.positions()) {
    if (!marked[pos]) {
      result.markTile(pos);
    }
  }
}

void StaticDeadTileInspectAllPushesCheckingAlgorithm::markDeadTiles(const Level &level, TileMarkings& result)
{
  // find all reachable states, considering only a single crate at a time.  Still, use the
  // starting position of every crate of the level.  The player position of the state can
  // be normalized to the top most and left most reachable tile of the state, to reduce the
  // number of duplicated states.

  // For every possible state, see if the crate can be pushed to any of the final storage
  // location.  This is done by looking at a single state at a item.  If the crate of the
  // state can be pushed to a "good" tile, mark the tile on which this crate is positioned
  // as "good".  If it can only pushed to "bad" tiles, mark the tile bad.  If one of the
  // tiles to which the crate can be pushed is neither good, nor bad, revisit the state
  // later.  In the beginning, mark every tile as neither good, nor bad, and mark all the
  // final storage locations as "good".

  // To speed up the process, another algorithm can be used before this one, to mark some
  // of the tiles as "bad" right from the beginnig.  For instance, we know that every corner
  // tile is "bad", since a crate can never be removed from the tile ever.  If this is done,
  // a state should be dropped from the queue, if the tile on which the crate of the state
  // is located is marked as "bad".

  // Since there are multiple crates in the level and each crate has a different starting
  // position, for one positon any of the crates can have, multiple player positions are
  // possible.
  States states(level);
  findAllPossibleStates(states, level);
  markUnreachableStates(states, level, result);
}

void StaticDeadTileInspectAllPushesCheckingAlgorithm::findAllPossibleStates(States& states, const Level& level) const
{
  std::queue<std::pair<LPos, LPos>> queue;
  for (const auto& pos : level.positions()) {
    if (level.isCrateAt(pos)) {
      queue.push({pos, level.playerPosition()});
    }
  }
  LData<bool> reachable(level.length());
  while (!queue.empty()) {
    const auto& cur = queue.front();
    const auto& cratePos = cur.first;
    const auto& playerPos = normalizePlayerPosition(level, cratePos, cur.second);
    if (!states.CrateStates[cratePos] || !states.playerPositionAvailable(cratePos, playerPos)) {
      states.CrateStates[cratePos] = true;
      states.addPlayerPosition(cratePos, playerPos);
      reachablePositions(level.width(), level.height(), playerPos,
                         [&](const LPos& pos) {
                           return !level.isWallAt(pos) && pos != cratePos;
                         },
                         reachable);
      for (const auto& nextCratePos : {cratePos - 1, cratePos + 1, cratePos - level.width(), cratePos + level.width()}) {
        const LPos playerPushPos = cratePos + cratePos.difference(nextCratePos);
        if (reachable[playerPushPos] && !level.isWallAt(nextCratePos)) {
          queue.push({nextCratePos, playerPushPos});
        }
      }
    }
    queue.pop();
  }
}

void StaticDeadTileInspectAllPushesCheckingAlgorithm::markUnreachableStates(const States& states, const Level& level, TileMarkings& result) const
{
  LData<Mark> levelMarks(level.length());
  std::fill(levelMarks.begin(), levelMarks.end(), Unmarked);

  for (const auto pos : level.positions()) {
    if (result.isTileMarked(pos) || !states.CrateStates[pos]) {
      levelMarks[pos] = Bad;
    } else if (level.isSpotAt(pos)) {
      levelMarks[pos] = Good;
    }
  }

  bool didChange = true;
  while (didChange) {
    didChange = false;
    for (const auto& pos : level.positions()) {
      if (levelMarks[pos] != Good) {
        continue;
      }
      // pos is now a good position. Check, if any of its neighbours is Unmarked.
      // If so, position the  on it an see if there is any state from which we can push the crate to this pos.
      for (const auto& neighbourCrate : { pos - 1, pos + 1, pos - level.width(), pos + level.width() }) {
        if (states.CrateStates[neighbourCrate] && levelMarks[neighbourCrate] == Unmarked) {
          const auto& playerPushPos = neighbourCrate + neighbourCrate.difference(pos);
          if (!level.isWallAt(playerPushPos)
              && states.playerPositionAvailable(neighbourCrate, normalizePlayerPosition(level, neighbourCrate, playerPushPos))) {
            didChange = true;
            levelMarks[neighbourCrate] = Good;
          }
        }
      }
    }
  }

  for (const auto& pos : level.positions()) {
    if (levelMarks[pos] != Good) {
      result.markTile(pos);
    }
  }
}

LPos StaticDeadTileInspectAllPushesCheckingAlgorithm::normalizePlayerPosition(const Level& level, const LPos& cratePos, const LPos& playerPos) const
{
  LData<bool> reachable(level.length());
  reachablePositions(level.width(), level.height(), playerPos,
                     [&](const LPos& pos) {
                       return !level.isWallAt(pos) && pos != cratePos;
                     },
                     reachable);
  for (const auto& pos : level.positions()) {
    if (reachable[pos]) {
      return pos;
    }
  }
  UNREACHABLE();
}

bool StaticDeadTileInspectAllPushesCheckingAlgorithm::States::addPlayerPosition(const LPos& cratePos, const LPos& playerPos)
{
  for (auto& storedPosition : PlayerPositions[cratePos]) {
    if (storedPosition == playerPos) {
      return false;
    }
  }
  PlayerPositions[cratePos].push_back(playerPos);
  return true;
}

bool StaticDeadTileInspectAllPushesCheckingAlgorithm::States::playerPositionAvailable(const LPos& cratePos, const LPos& playerPos) const
{
  for (const auto& storedPosition : PlayerPositions[cratePos]) {
    if (storedPosition == playerPos) {
      return true;
    }
  }
  return false;
}

void DynamicDeadTileNoOpCheckingAlgorithm::configureFor(const Level& level)
{
  (void) level;
}

bool DynamicDeadTileNoOpCheckingAlgorithm::isCrateOnDeadTile(const Level& level,
                                                             const LPos& pos)
{
  (void) level;
  (void) pos;
  return false;
}

void DynamicDeadTileTestNeighboursCheckingAlgorithm::configureFor(const Level& level)
{
  StaticDeadSpots = TileMarkings(level);
  CrateStates = LData<CrateState>(level.length());
}


bool DynamicDeadTileTestNeighboursCheckingAlgorithm::isCrateOnDeadTile(const Level& level,
                                                                       const LPos& crateToCheckPos)
{
  ASSERT(crateToCheckPos < level.length(), "cratePos out of range");
  ASSERT(level.isCrateAt(crateToCheckPos), "can only check positions with a crate");
  ASSERT(CrateStates.length() > 0, "need to configure before use");
  ASSERT(StaticDeadSpots.numberOfTiles() == level.length(), "need to configure before use");
  // If there is no crate next to the given one,
  // it can not be on a dynamic dead tile, only on a static dead one.
  if (level.isCrateAt(crateToCheckPos - 1) ||
      level.isCrateAt(crateToCheckPos + 1) ||
      level.isCrateAt(crateToCheckPos - level.width()) ||
      level.isCrateAt(crateToCheckPos + level.width())) {
    std::fill(CrateStates.begin(), CrateStates.end(), NoCrate);
    for (size_t crateIndex = 0; crateIndex < level.numberOfCrates(); ++crateIndex) {
      CrateStates[level.crateAt(crateIndex)] = CrateState::Unknown;
    }
    if (isCrateBlocked(level, crateToCheckPos)) {
      // The crate created a dead lock with the chain of its adjecent crates.
      // But some of them might be on spots.
      // If all of them are on spots, it is not a dead lock!
      for (size_t crateIndex = 0; crateIndex < level.numberOfCrates(); ++crateIndex) {
        const auto& cratePos = level.crateAt(crateIndex);
        ASSERT(CrateStates[cratePos] != CrateState::NoCrate &&
               CrateStates[cratePos] != CrateState::InProgress,
               "invariant");
        if (CrateStates[cratePos] == Blocked && !level.isSpotAt(cratePos)) {
          return true;
        }
      }
    }
  }
  return false;
}

bool DynamicDeadTileTestNeighboursCheckingAlgorithm::isCrateBlocked(const Level& level,
                                                                    const LPos& cratePos)
{
  ASSERT(cratePos < level.length(), "cratePos out of range");
  ASSERT(level.isCrateAt(cratePos), "cratePos must point to a crate");
  ASSERT(CrateStates[cratePos] == Unknown, "invariant");
  CrateStates[cratePos] = InProgress;
  const auto width = level.width();
  // If both planes, the horizontal and vertical, are blocked, the crate is blocked
  const bool isBlocked =
    isPlaneBlocked(level, cratePos - 1, cratePos + 1) &&
    isPlaneBlocked(level, cratePos - width, cratePos + width);
  CrateStates[cratePos] = (isBlocked) ? Blocked : NotBlocked;
  return Blocked == CrateStates[cratePos];
}

inline
bool DynamicDeadTileTestNeighboursCheckingAlgorithm::isPlaneBlocked(const Level& level,
                                                                    const LPos& firstSide,
                                                                    const LPos& secondSide)
{
  // This following check is done first horizontally, then vertically:
  //  - If there is a wall on either side, the plane is blocked
  //  - If both sides are dead spots, the plane is blocked
  //  - If the crate to either side is blocked, the plane is blocked
  //     - If the crate on its side has no yet been visited, check it recursively
  //     - If the crate on its side has been visited, but the check did not yet complete
  //       consider it blocked
  //     - Otherwise take the computed value
  if (level.isWallAt(firstSide) || level.isWallAt(secondSide)) {
    return true;
  }
  if (StaticDeadSpots.isTileMarked(firstSide) && StaticDeadSpots.isTileMarked(secondSide)) {
    return true;
  }
  if (CrateState::NoCrate != CrateStates[firstSide]) {
    if ((CrateState::Unknown == CrateStates[firstSide] &&
         isCrateBlocked(level, firstSide)) ||
        NotBlocked != CrateStates[firstSide]) {
      return true;
    }
  }
  if (CrateState::NoCrate != CrateStates[secondSide]) {
    return (CrateState::Unknown == CrateStates[secondSide])
      ? isCrateBlocked(level, secondSide)
      : NotBlocked != CrateStates[secondSide];
  }
  return false;
}
