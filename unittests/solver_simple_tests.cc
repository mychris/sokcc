/* SPDX-License-Identifier: MIT */
#include "dead_spot.h"
#include "solver.h"
#include "level.h"
#include "lurd.h"

#include <gtest/gtest.h>

class SolverTest : public ::testing::Test
{
};

TEST_F(SolverTest, AlreadySolvedLevel)
{
  const char* levelString =
    "####\n"
    "#@*#\n"
    "####\n";
  auto solver = Solver::Builder()
    .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
    .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
    .build<SimpleBreadthFirstSolver>();

  auto level = Level::parseFromString(levelString);
  auto solution = solver->solveLevel(level);
  ASSERT_TRUE(solution.empty());
  ASSERT_EQ(0, solution.size());
  ASSERT_EQ(0, solution.numberOfMoves());
  ASSERT_EQ(0, solution.numberOfPushes());
}

TEST_F(SolverTest, SingleStraightPush)
{
  const char* levelString =
    "#####\n"
    "#@$.#\n"
    "#####\n";
  auto solver = Solver::Builder()
    .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
    .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
    .build<SimpleBreadthFirstSolver>();

  auto level = Level::parseFromString(levelString);
  auto solution = solver->solveLevel(level);
  ASSERT_FALSE(solution.empty());
  ASSERT_EQ(1, solution.size());
  ASSERT_EQ(1, solution.numberOfMoves());
  ASSERT_EQ(1, solution.numberOfPushes());
}

TEST_F(SolverTest, MultipleStraightPushes)
{
  const char* levelString =
    "########\n"
    "#@ $  .#\n"
    "########\n";
  auto solver = Solver::Builder()
    .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
    .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
    .build<SimpleBreadthFirstSolver>();

  auto level = Level::parseFromString(levelString);
  auto solution = solver->solveLevel(level);
  ASSERT_FALSE(solution.empty());
  ASSERT_EQ(4, solution.size());
  ASSERT_EQ(4, solution.numberOfMoves());
  ASSERT_EQ(3, solution.numberOfPushes());
}

/*
https://sokoban-solver-statistics.sourceforge.io/game/?json=ew0KICAiQm9hcmQiOiBbDQogICIjIyMjIyMjIyIsDQogICIjQCAgIyAgIyIsDQogICIjIC4uICAgIyMiLA0KICAiIyMuIyMgICAjIiwNCiAgIiAjICAkJCMgIyIsDQogICIgIyMjJCAgICMiLA0KICAiICAgIyAgIyMjIiwNCiAgIiAgICMjIyMiDQogIF0sDQogICJMZXZlbCBTZXQiOiAiU29sdmVyIFN0YXRpc3RpY3MgTGV2ZWwiLA0KICAiTGV2ZWwgVGl0bGUiOiAiQXltZXJpYyBEdSBQZWxvdXggfCBMZXZlbCAxIiwNCiAgIkxldmVsIE5vLiI6IDENCn0=

https://www.letslogic.com/level/10028
*/
TEST_F(SolverTest, Cosmonotes_1)
{
  const char* levelString =
    "########-\n"
    "#@  #  #-\n"
    "# ..   ##\n"
    "##.##   #\n"
    "-#  $$# #\n"
    "-###$   #\n"
    "---#  ###\n"
    "---####--\n";

  auto solver = Solver::Builder()
    .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
    .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
    .build<SimpleBreadthFirstSolver>();

  auto level = Level::parseFromString(levelString);
  auto solution = solver->solveLevel(level);
  ASSERT_FALSE(solution.empty());
  ASSERT_EQ(146, solution.size());
  ASSERT_EQ(146, solution.numberOfMoves());
  ASSERT_EQ(42, solution.numberOfPushes());
}

/*
https://sokoban-solver-statistics.sourceforge.io/game/?json=ew0KICAiQm9hcmQiOiBbDQogICIgIyMjIyIsDQogICIgIyAgIyMjIyMjIiwNCiAgIiAjICAgICAgICMiLA0KICAiIyMgIyMjIyAgIyIsDQogICIjQCogIyMjICMjIiwNCiAgIiMgICogIyMgIyIsDQogICIjIyAgKiAjICMjIiwNCiAgIiAjIyAuJCMgICMiLA0KICAiICAjIyAgICAgIyIsDQogICIgICAjIyMjICAjIiwNCiAgIiAgICAgICMjIyMiDQogIF0sDQogICJMZXZlbCBTZXQiOiAiU29sdmVyIFN0YXRpc3RpY3MgTGV2ZWwiLA0KICAiTGV2ZWwgVGl0bGUiOiAiQXltZXJpYyBEdSBQZWxvdXggfCBMZXZlbCAyIiwNCiAgIkxldmVsIE5vLiI6IDINCn0=

https://www.letslogic.com/level/10029
*/
TEST_F(SolverTest, Cosmonotes_2)
{
  const char* levelString =
    "-####-----\n"
    "-#  ######\n"
    "-#       #\n"
    "## ####  #\n"
    "#@* ### ##\n"
    "#  * ## #-\n"
    "##  * # ##\n"
    "-## .$#  #\n"
    "--##     #\n"
    "---####  #\n"
    "------####\n";

  auto solver = Solver::Builder()
    .usingStaticDeadSpotDetection(std::make_unique<StaticDeadTileInspectAllPushesCheckingAlgorithm>())
    .usingDynamicDeadSpotChecker(std::make_unique<DynamicDeadTileTestNeighboursCheckingAlgorithm>())
    .build<SimpleBreadthFirstSolver>();

  auto level = Level::parseFromString(levelString);
  auto solution = solver->solveLevel(level);
  ASSERT_FALSE(solution.empty());
  ASSERT_EQ(292, solution.size());
  ASSERT_EQ(292, solution.numberOfMoves());
  ASSERT_EQ(87, solution.numberOfPushes());
}
