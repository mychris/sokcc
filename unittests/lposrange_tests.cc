/* SPDX-License-Identifier: MIT */
#include "types.h"
#include <gtest/gtest.h>
#include <random>

class LPosRangeTest : public ::testing::Test {};

TEST_F(LPosRangeTest, EmptyRange)
{
  size_t i;
  i = 0;
  for (const auto& pos : LPosRange(LPos(9, 10), 0)) {
    (void) pos;
    ++i;
  }
  ASSERT_EQ(i, 0);

  i = 0;
  for (const auto& pos : LPosRange(LPos(0, 10), 0)) {
    (void)pos;
    ++i;
  }
  ASSERT_EQ(i, 0);
}

TEST_F(LPosRangeTest, OneStepRange)
{
  size_t i;
  i = 0;
  for (const auto& pos : LPosRange(LPos(9, 10), 1)) {
    ASSERT_EQ(LPos(9, 10), pos);
    ++i;
  }
  ASSERT_EQ(i, 1);

  i = 0;
  for (const auto& pos : LPosRange(LPos(0, 10), 1)) {
    ASSERT_EQ(LPos(0, 10), pos);
    ++i;
  }
  ASSERT_EQ(i, 1);
}

TEST_F(LPosRangeTest, TwoStepRange)
{
  size_t i;
  i = 0;
  for (const auto& pos : LPosRange(LPos(8, 10), 2)) {
    ASSERT_EQ(LPos(8 + i, 10), pos);
    ++i;
  }
  ASSERT_EQ(i, 2);

  i = 0;
  for (const auto &pos : LPosRange(LPos(0, 10), 2)) {
    ASSERT_EQ(LPos(i, 10), pos);
    ++i;
  }
  ASSERT_EQ(i, 2);

  ASSERT_DEATH({
    for (const auto& pos : LPosRange(LPos(9, 10), 2)) {
      (void) pos;
    }
   }, "");
}

TEST_F(LPosRangeTest, FullRangeManualPrefixIncrement)
{
  LPos expected(0, 5);
  LPosRange range(LPos(0, 5), 5);
  auto it = range.begin();
  auto end = range.end();
  while (it != end) {
    ASSERT_EQ(expected, *it);
    ++it;
    if (it != end) {
      ++expected;
    }
  }
}

TEST_F(LPosRangeTest, FullRangeManualPostfixIncrement)
{
  LPos expected(0, 5);
  LPosRange range(LPos(0, 5), 5);
  auto it = range.begin();
  auto end = range.end();
  if (it != end) {
    for (;;) {
      ASSERT_EQ(expected, *(it++));
      if (it == end) {
        break;
      }
      expected++;
      ASSERT_EQ(expected, *it);
    }
  }
}

TEST_F(LPosRangeTest, FullRange)
{
  size_t i = 0;
  for (const auto& pos : LPosRange(LPos(0, 10), 10)) {
    ASSERT_EQ(LPos(i, 10), pos);
    ++i;
  }
  ASSERT_EQ(i, 10);

  ASSERT_DEATH({
    for (const auto& pos : LPosRange(LPos(0, 10), 11)) {
      (void) pos;
    }
  }, "");
}
