/* SPDX-License-Identifier: MIT */
#include "types.h"
#include <gtest/gtest.h>
#include <random>

class LPosTest : public ::testing::Test {};

TEST_F(LPosTest, ConstructInvalid)
{
  ASSERT_DEATH({
    LPos pos(10, 10);
  }, "Invalid.*");
}

TEST_F(LPosTest, PrefixIncrement)
{
  LPos pos(0, 10);
  ASSERT_EQ(LPos(0, 10), pos);
  ++pos;
  ASSERT_EQ(LPos(1, 10), pos);
  ASSERT_EQ(LPos(2, 10), ++pos);
  for (size_t i = 3; i < 10; ++i) {
    ASSERT_EQ(LPos(i, 10), ++pos);
  }
}

TEST_F(LPosTest, PrefixIncrementPastEnd)
{
  ASSERT_DEATH({
    LPos pos(9, 10);
    ++pos;
  }, ".*overflow.*");
}

TEST_F(LPosTest, PostfixIncrement)
{
  LPos pos(0, 10);
  ASSERT_EQ(LPos(0, 10), pos);
  pos++;
  ASSERT_EQ(LPos(1, 10), pos);
  ASSERT_EQ(LPos(1, 10), pos++);
  ASSERT_EQ(LPos(2, 10), pos);
  for (size_t i = 2; i < 9; ++i) {
    ASSERT_EQ(LPos(i, 10), pos++);
  }
  ASSERT_EQ(LPos(9, 10), pos);
}

TEST_F(LPosTest, PostfixIncrementPastEnd)
{
  ASSERT_DEATH({
    LPos pos(9, 10);
    pos++;
  }, ".*overflow.*");
}

TEST_F(LPosTest, PrefixDecrement)
{
  LPos pos(9, 10);
  ASSERT_EQ(LPos(9, 10), pos);
  --pos;
  ASSERT_EQ(LPos(8, 10), pos);
  ASSERT_EQ(LPos(7, 10), --pos);
  for (size_t i = 4; i <= 10; ++i) {
    ASSERT_EQ(LPos(10 - i, 10), --pos);
  }
}

TEST_F(LPosTest, PrefixDecrementPastBeginning)
{
  ASSERT_DEATH({
    LPos pos(0, 10);
    --pos;
  }, ".*(overflow|underflow).*");
}

TEST_F(LPosTest, PostfixDecrement)
{
  LPos pos(9, 10);
  ASSERT_EQ(LPos(9, 10), pos);
  pos--;
  ASSERT_EQ(LPos(8, 10), pos);
  ASSERT_EQ(LPos(8, 10), pos--);
  ASSERT_EQ(LPos(7, 10), pos);
  for (size_t i = 3; i < 10; ++i) {
    ASSERT_EQ(LPos(10 - i, 10), pos--);
  }
  ASSERT_EQ(LPos(0, 10), pos);
}

TEST_F(LPosTest, PostfixDecrementPastBeginning)
{
  ASSERT_DEATH({
    LPos pos(0, 10);
    pos--;
  }, ".*(overflow|underflow).*");
}

TEST_F(LPosTest, Add)
{
  LPos pos(0, 100);
  ASSERT_EQ(LPos(0, 100), pos);
  ASSERT_EQ(LPos(5, 100), pos + LPos(5, 100));
  ASSERT_EQ(LPos(5, 100), pos + static_cast<std::size_t>(5u));
  ASSERT_EQ(LPos(5, 100), pos + static_cast<int>(5));
  pos += LPos(5, 100);
  ASSERT_EQ(LPos(5, 100), pos);
  pos += static_cast<std::size_t>(5u);
  ASSERT_EQ(LPos(10, 100), pos);
  pos += static_cast<int>(5);
  ASSERT_EQ(LPos(15, 100), pos);
  ASSERT_EQ(LPos(20, 100), pos += LPos(5, 100));
  ASSERT_EQ(LPos(25, 100), pos += static_cast<std::size_t>(5u));
  ASSERT_EQ(LPos(30, 100), pos += static_cast<int>(5));

  ASSERT_EQ(LPos(99, 100), LPos(77, 100) + LPos(22, 100));
  ASSERT_EQ(LPos(99, 100), LPos(77, 100) + static_cast<std::size_t>(22u));
  ASSERT_EQ(LPos(99, 100), LPos(77, 100) + static_cast<int>(22));

  ASSERT_EQ(LPos(99, 100), LPos(55, 100) + LPos(44, 100));
  ASSERT_EQ(LPos(99, 100), LPos(55, 100) + static_cast<std::size_t>(44u));
  ASSERT_EQ(LPos(99, 100), LPos(55, 100) + static_cast<int>(44));

  ASSERT_EQ(LPos(0, 100), LPos(55, 100) + static_cast<int>(-55));
}

TEST_F(LPosTest, AddOutOfRange)
{
  ASSERT_DEATH({
    LPos(57, 100) + LPos(43, 100);
  }, ".*overflow.*");

  ASSERT_DEATH({
    LPos(57, 100) + static_cast<std::size_t>(43);
  }, ".*overflow.*");

  ASSERT_DEATH({
    LPos(57, 100) + static_cast<int>(43);
  }, ".*overflow.*");

  ASSERT_DEATH({
    LPos(57, 100) + static_cast<int>(-58);
  }, ".*(overflow|underflow).*");
}

TEST_F(LPosTest, Sub)
{
  LPos pos(90, 100);
  ASSERT_EQ(LPos(90, 100), pos);
  ASSERT_EQ(LPos(85, 100), pos - LPos(5, 100));
  ASSERT_EQ(LPos(85, 100), pos - static_cast<std::size_t>(5u));
  ASSERT_EQ(LPos(85, 100), pos - static_cast<int>(5));
  pos -= LPos(5, 100);
  ASSERT_EQ(LPos(85, 100), pos);
  pos -= static_cast<std::size_t>(5u);
  ASSERT_EQ(LPos(80, 100), pos);
  pos -= static_cast<int>(5);
  ASSERT_EQ(LPos(75, 100), pos);
  ASSERT_EQ(LPos(70, 100), pos -= LPos(5, 100));
  ASSERT_EQ(LPos(65, 100), pos -= static_cast<std::size_t>(5u));
  ASSERT_EQ(LPos(60, 100), pos -= static_cast<int>(5));

  ASSERT_EQ(LPos(77, 100), LPos(99, 100) - LPos(22, 100));
  ASSERT_EQ(LPos(77, 100), LPos(99, 100) - static_cast<std::size_t>(22u));
  ASSERT_EQ(LPos(77, 100), LPos(99, 100) - static_cast<int>(22));

  ASSERT_EQ(LPos(55, 100), LPos(99, 100) - LPos(44, 100));
  ASSERT_EQ(LPos(55, 100), LPos(99, 100) - static_cast<std::size_t>(44u));
  ASSERT_EQ(LPos(55, 100), LPos(99, 100) - static_cast<int>(44));

  ASSERT_EQ(LPos(55, 100), LPos(0, 100) - static_cast<int>(-55));
}

TEST_F(LPosTest, SubOutOfRange)
{
  ASSERT_DEATH({
    LPos(42, 100) - LPos(43, 100);
  }, ".*(overflow|underflow).*");

  ASSERT_DEATH({
    LPos(42, 100) - static_cast<std::size_t>(43);
  }, ".*(overflow|underflow).*");

  ASSERT_DEATH({
    LPos(42, 100) - static_cast<int>(43);
  }, ".*(overflow|underflow).*");

  ASSERT_DEATH({
    LPos(57, 100) - static_cast<int>(-43);
  }, ".*(overflow|underflow).*");
}

static inline void check_eq(const LPos& left, const LPos& right)
{
  ASSERT_EQ(left, right);
  ASSERT_EQ(left, static_cast<size_t>(right.get()));
  ASSERT_EQ(left, static_cast<int>(right.get()));
  ASSERT_EQ(right, left);
  ASSERT_EQ(static_cast<size_t>(right.get()), left);
  ASSERT_EQ(static_cast<int>(right.get()), left);
  ASSERT_LE(left, right);
  ASSERT_LE(left, static_cast<size_t>(right.get()));
  ASSERT_LE(left, static_cast<int>(right.get()));
  ASSERT_LE(right, left);
  ASSERT_LE(static_cast<size_t>(right.get()), left);
  ASSERT_LE(static_cast<int>(right.get()), left);
  ASSERT_GE(left, right);
  ASSERT_GE(left, static_cast<size_t>(right.get()));
  ASSERT_GE(left, static_cast<int>(right.get()));
  ASSERT_GE(right, left);
  ASSERT_GE(static_cast<size_t>(right.get()), left);
  ASSERT_GE(static_cast<int>(right.get()), left);
}

static inline void check_lt(const LPos& left, const LPos& right)
{
  ASSERT_LT(left, right);
  ASSERT_LT(left, static_cast<size_t>(right.get()));
  ASSERT_LT(left, static_cast<int>(right.get()));
  ASSERT_LE(left, right);
  ASSERT_LE(left, static_cast<size_t>(right.get()));
  ASSERT_LE(left, static_cast<int>(right.get()));

  ASSERT_GT(right, left);
  ASSERT_GT(static_cast<size_t>(right.get()), left);
  ASSERT_GT(static_cast<int>(right.get()), left);
  ASSERT_GE(right, left);
  ASSERT_GE(static_cast<size_t>(right.get()), left);
  ASSERT_GE(static_cast<int>(right.get()), left);

  ASSERT_NE(left, right);
  ASSERT_NE(left, static_cast<size_t>(right.get()));
  ASSERT_NE(left, static_cast<int>(right.get()));
  ASSERT_NE(right, left);
  ASSERT_NE(static_cast<size_t>(right.get()), left);
  ASSERT_NE(static_cast<int>(right.get()), left);
}

static inline void check_gt(const LPos& left, const LPos& right)
{
  ASSERT_GT(left, right);
  ASSERT_GT(left, static_cast<size_t>(right.get()));
  ASSERT_GT(left, static_cast<int>(right.get()));
  ASSERT_GE(left, right);
  ASSERT_GE(left, static_cast<size_t>(right.get()));
  ASSERT_GE(left, static_cast<int>(right.get()));

  ASSERT_LT(right, left);
  ASSERT_LT(static_cast<size_t>(right.get()), left);
  ASSERT_LT(static_cast<int>(right.get()), left);
  ASSERT_LE(right, left);
  ASSERT_LE(static_cast<size_t>(right.get()), left);
  ASSERT_LE(static_cast<int>(right.get()), left);

  ASSERT_NE(left, right);
  ASSERT_NE(left, static_cast<size_t>(right.get()));
  ASSERT_NE(left, static_cast<int>(right.get()));
  ASSERT_NE(right, left);
  ASSERT_NE(static_cast<size_t>(right.get()), left);
  ASSERT_NE(static_cast<int>(right.get()), left);
}

TEST_F(LPosTest, Comparison)
{
  LPos left(0, 100);
  LPos right(0, 100);
  check_eq(left, right);

  for (int i : { 1, 2, 3, 4, 5, 22, 6, 55, 55, 8, 7, 8, 99, 11, 43, 52, 74, 67 }) {
    right = i;
    size_t posValue = static_cast<size_t>(i);
    if (left.get() == posValue) {
      check_eq(left, right);
    } else if (left.get() < posValue) {
      check_lt(left, right);
    } else if (left.get() > posValue) {
      check_gt(left, right);
    } else {
      FAIL() << "We shouldn't get here.";
    }
    left = right;
  }

  ASSERT_NE(LPos(5, 100), static_cast<int>(-3));
  ASSERT_NE(static_cast<int>(-3), LPos(5, 100));
  ASSERT_FALSE(LPos(5, 100) == static_cast<int>(-3));
  ASSERT_FALSE(static_cast<int>(-3) == LPos(5, 100));
  ASSERT_LT(static_cast<int>(-3), LPos(5, 100));
  ASSERT_FALSE(LPos(5, 100) < static_cast<int>(-3));
  ASSERT_GT(LPos(5, 100), static_cast<int>(-3));
  ASSERT_FALSE(static_cast<int>(-3) > LPos(5, 100));
  ASSERT_LE(static_cast<int>(-3), LPos(5, 100));
  ASSERT_FALSE(LPos(5, 100) <= static_cast<int>(-3));
  ASSERT_GE(LPos(5, 100), static_cast<int>(-3));
  ASSERT_FALSE(static_cast<int>(-3) >= LPos(5, 100));
}
