/* SPDX-License-Identifier: MIT */
#include "dead_spot.h"
#include "level.h"
#include "tile_marking.h"
#include <gtest/gtest.h>
#include <string>

class DynamicDeadSpotBaseTest : public ::testing::Test
{
protected:
  Level Lvl;

  explicit DynamicDeadSpotBaseTest(const std::string& levelString)
    : Lvl(Level::parseFromString(levelString))
  {
  }
};

class DynamicDeadSpotSingleFreeCrateTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotSingleFreeCrateTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotSingleFreeCrateTest::LvlString =
  "#######\n"
  "#     #\n"
  "# @$. #\n"
  "#     #\n"
  "#######\n";

TEST_F(DynamicDeadSpotSingleFreeCrateTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(17, Lvl.length())));
}

TEST_F(DynamicDeadSpotSingleFreeCrateTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(17, Lvl.length())));
}

class DynamicDeadSpotSingleCrateOnSpotTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotSingleCrateOnSpotTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotSingleCrateOnSpotTest::LvlString =
  "#######\n"
  "#     #\n"
  "# @*  #\n"
  "#     #\n"
  "#######\n";

TEST_F(DynamicDeadSpotSingleCrateOnSpotTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(17, Lvl.length())));
}

TEST_F(DynamicDeadSpotSingleCrateOnSpotTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(17, Lvl.length())));
}

class DynamicDeadSpotTwoFreeCratesTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoFreeCratesTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoFreeCratesTest::LvlString =
  "#########\n"
  "#       #\n"
  "# @     #\n"
  "# $ $ . #\n"
  "#     . #\n"
  "#########\n";

TEST_F(DynamicDeadSpotTwoFreeCratesTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(29, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoFreeCratesTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(29, Lvl.length())));
}

class DynamicDeadSpotTwoHorizontallyBlockedCratesTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoHorizontallyBlockedCratesTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoHorizontallyBlockedCratesTest::LvlString =
  "########\n"
  "#      #\n"
  "# @    #\n"
  "#$$#   #\n"
  "#..#   #\n"
  "########\n";

TEST_F(DynamicDeadSpotTwoHorizontallyBlockedCratesTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(26, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoHorizontallyBlockedCratesTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(26, Lvl.length())));
}

class DynamicDeadSpotTwoVerticallyBlockedCratesTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoVerticallyBlockedCratesTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoVerticallyBlockedCratesTest::LvlString =
  "######\n"
  "#.$  #\n"
  "#.$@ #\n"
  "######\n";

TEST_F(DynamicDeadSpotTwoVerticallyBlockedCratesTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(14, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoVerticallyBlockedCratesTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(14, Lvl.length())));
}


class DynamicDeadSpotTwoCratesPushedTogetherNextToWallTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoCratesPushedTogetherNextToWallTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoCratesPushedTogetherNextToWallTest::LvlString =
  "###########\n"
  "#   $$    #\n"
  "# .  @  . #\n"
  "#         #\n"
  "###########\n";

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

class DynamicDeadSpotTwoCratesPushedTogetherNextToWallButOnSpotsTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoCratesPushedTogetherNextToWallButOnSpotsTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoCratesPushedTogetherNextToWallButOnSpotsTest::LvlString =
  "###########\n"
  "#   **    #\n"
  "#    @    #\n"
  "#         #\n"
  "###########\n";

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallButOnSpotsTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallButOnSpotsTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

class DynamicDeadSpotTwoCratesPushedTogetherNextToWallLeftOnSpotsTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoCratesPushedTogetherNextToWallLeftOnSpotsTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoCratesPushedTogetherNextToWallLeftOnSpotsTest::LvlString =
  "###########\n"
  "#   *$    #\n"
  "#    @    #\n"
  "# .       #\n"
  "###########\n";

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallLeftOnSpotsTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallLeftOnSpotsTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

class DynamicDeadSpotTwoCratesPushedTogetherNextToWallRightOnSpotsTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotTwoCratesPushedTogetherNextToWallRightOnSpotsTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotTwoCratesPushedTogetherNextToWallRightOnSpotsTest::LvlString =
  "###########\n"
  "#   $*    #\n"
  "#    @    #\n"
  "# .       #\n"
  "###########\n";

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallRightOnSpotsTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

TEST_F(DynamicDeadSpotTwoCratesPushedTogetherNextToWallRightOnSpotsTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(16, Lvl.length())));
}

class DynamicDeadSpotBlockedButSolvedTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotBlockedButSolvedTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotBlockedButSolvedTest::LvlString =
  "########\n"
  "#   #  #\n"
  "#  **@ #\n"
  "# **   #\n"
  "###    #\n"
  "########\n";

TEST_F(DynamicDeadSpotBlockedButSolvedTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

TEST_F(DynamicDeadSpotBlockedButSolvedTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}


class DynamicDeadSpotComplexLevelTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotComplexLevelTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotComplexLevelTest::LvlString =
  "########\n"
  "#   #  #\n"
  "#  **@ #\n"
  "#.$*   #\n"
  "###    #\n"
  "########\n";

TEST_F(DynamicDeadSpotComplexLevelTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

TEST_F(DynamicDeadSpotComplexLevelTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

class DynamicDeadSpotComplexLevelRotatedTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotComplexLevelRotatedTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotComplexLevelRotatedTest::LvlString =
  "######\n"
  "#    #\n"
  "# @  #\n"
  "##*  #\n"
  "# ** #\n"
  "#  $##\n"
  "#  .##\n"
  "######\n";

TEST_F(DynamicDeadSpotComplexLevelRotatedTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

TEST_F(DynamicDeadSpotComplexLevelRotatedTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

class DynamicDeadSpotComplexLevelHorizontallyFreeTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotComplexLevelHorizontallyFreeTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotComplexLevelHorizontallyFreeTest::LvlString =
  "########\n"
  "#.  $@ #\n"
  "#  $$ .#\n"
  "#.$$   #\n"
  "###..  #\n"
  "########\n";

TEST_F(DynamicDeadSpotComplexLevelHorizontallyFreeTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(12, Lvl.length())));
}

TEST_F(DynamicDeadSpotComplexLevelHorizontallyFreeTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(12, Lvl.length())));
}

class DynamicDeadSpotComplexLevelVerticallyFreeTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotComplexLevelVerticallyFreeTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotComplexLevelVerticallyFreeTest::LvlString =
  "#######\n"
  "####. #\n"
  "#. $$ #\n"
  "#.$$@ #\n"
  "###.  #\n"
  "#######\n";

TEST_F(DynamicDeadSpotComplexLevelVerticallyFreeTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(18, Lvl.length())));
}

TEST_F(DynamicDeadSpotComplexLevelVerticallyFreeTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(18, Lvl.length())));
}

class DynamicDeadSpotOneFreeButConnectedTest : public DynamicDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  DynamicDeadSpotOneFreeButConnectedTest() : DynamicDeadSpotBaseTest(LvlString) {}
};
const std::string DynamicDeadSpotOneFreeButConnectedTest::LvlString =
  " #######\n"
  " #     #\n"
  " #$$$@ #\n"
  "## ### #\n"
  "#      #\n"
  "# ...  #\n"
  "#######\n";

TEST_F(DynamicDeadSpotOneFreeButConnectedTest, NoOpAlgorithm)
{
  DynamicDeadTileNoOpCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(19, Lvl.length())));
  EXPECT_FALSE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}

TEST_F(DynamicDeadSpotOneFreeButConnectedTest, TestNeighboursAlgorithm)
{
  DynamicDeadTileTestNeighboursCheckingAlgorithm algo;
  algo.configureFor(Lvl);
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(19, Lvl.length())));
  EXPECT_TRUE(algo.isCrateOnDeadTile(Lvl, LPos(20, Lvl.length())));
}
