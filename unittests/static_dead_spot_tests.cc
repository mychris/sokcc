/* SPDX-License-Identifier: MIT */
#include "algorithms.h"
#include "dead_spot.h"
#include "level.h"
#include "tile_marking.h"
#include <gtest/gtest.h>
#include <string>

class StaticDeadSpotBaseTest : public ::testing::Test
{
protected:
  Level Lvl;
  TileMarkings Actual;

  explicit StaticDeadSpotBaseTest(const std::string& levelString)
    : Lvl(Level::parseFromString(levelString))
    , Actual(Lvl)
  {
    Actual.unmarkAllTiles();
  }

  void printStaticDeadSpots(const TileMarkings& marked)
  {
    for (const auto& pos : Lvl.positions()) {
      if (pos && static_cast<size_t>(pos) % Lvl.width() == 0) {
        puts("");
      }
      if (marked.isTileMarked(pos)) {
        putchar('#');
      } else {
        putchar(' ');
      }
    }
    puts("");
  }

  ::testing::AssertionResult checkStaticDeadSpots(const TileMarkings& actual,
                                            const std::string& expectedString)
  {
    TileMarkings expected(Lvl);
    size_t pos = 0;
    for (char c : expectedString) {
      if (c != '\n' && c != '\r') {
        if (c != ' ') {
          expected.markTile(LPos(pos, Lvl.length()));
        }
        pos++;
      }
    }
    if (pos != Lvl.length()) {
      return ::testing::AssertionFailure() << "expected string too short";
    }
    TileMarkings tmp(actual);
    
    tmp.markAllTiles();
    if (tmp.numberOfMarkedTiles() != Lvl.length()) {
      return ::testing::AssertionFailure()
        << "actual has length " << tmp.numberOfMarkedTiles()
        << ", expected " << Lvl.length();
    }
    
    if (actual != expected) {
      return ::testing::AssertionFailure() << "dead spot mismatch";
    }

    return ::testing::AssertionSuccess();
  }
};

class StaticDeadSpotSmallThreeTileLevelTest : public StaticDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  StaticDeadSpotSmallThreeTileLevelTest() : StaticDeadSpotBaseTest(LvlString) {}
};
const std::string StaticDeadSpotSmallThreeTileLevelTest::LvlString =
  "#####\n"
  "#@$.#\n"
  "#####\n";

TEST_F(StaticDeadSpotSmallThreeTileLevelTest, NoOpAlgorithm)
{
  std::string expected =
    "#####\n"
    "#   #\n"
    "#####\n";
  StaticDeadTileNoOpCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallThreeTileLevelTest, CheckCornersAlgorithm)
{
  std::string expected =
    "#####\n"
    "##  #\n"
    "#####\n";
  StaticDeadTileCheckCornersCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallThreeTileLevelTest, SinglePushAlgorithm)
{
  std::string expected =
    "#####\n"
    "##  #\n"
    "#####\n";
  StaticDeadTileSinglePushCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallThreeTileLevelTest, InspectAllPushesAlgorithm)
{
  std::string expected =
    "#####\n"
    "##  #\n"
    "#####\n";
  StaticDeadTileInspectAllPushesCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

class StaticDeadSpotSmallSquareLevelTest : public StaticDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  StaticDeadSpotSmallSquareLevelTest() : StaticDeadSpotBaseTest(LvlString) {}
};
const std::string StaticDeadSpotSmallSquareLevelTest::LvlString =
  "#########\n"
  "#       #\n"
  "# @ $ . #\n"
  "#       #\n"
  "#########\n";

TEST_F(StaticDeadSpotSmallSquareLevelTest, NoOpAlgorithm)
{
  std::string expected =
    "#########\n"
    "#       #\n"
    "#       #\n"
    "#       #\n"
    "#########\n";
  StaticDeadTileNoOpCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallSquareLevelTest, CheckCornersAlgorithm)
{
  std::string expected =
    "#########\n"
    "##     ##\n"
    "#       #\n"
    "##     ##\n"
    "#########\n";
  StaticDeadTileCheckCornersCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallSquareLevelTest, SinglePushAlgorithm)
{
  std::string expected =
    "#########\n"
    "#########\n"
    "##     ##\n"
    "#########\n"
    "#########\n";
  StaticDeadTileSinglePushCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallSquareLevelTest, InspectAllPushesAlgorithm)
{
  std::string expected =
    "#########\n"
    "#########\n"
    "##     ##\n"
    "#########\n"
    "#########\n";
  StaticDeadTileInspectAllPushesCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

class StaticDeadSpotSmallCoveLevelTest : public StaticDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  StaticDeadSpotSmallCoveLevelTest() : StaticDeadSpotBaseTest(LvlString) {}
};
const std::string StaticDeadSpotSmallCoveLevelTest::LvlString =
  "  #####  \n"
  "###   ###\n"
  "# @ $   #\n"
  "#       #\n"
  "###  .###\n"
  "  #####  \n";

TEST_F(StaticDeadSpotSmallCoveLevelTest, NoOpAlgorithm)
{
  std::string expected =
    "#########\n"
    "###   ###\n"
    "#       #\n"
    "#       #\n"
    "###   ###\n"
    "#########\n";
  StaticDeadTileNoOpCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallCoveLevelTest, CheckCornersAlgorithm)
{
  std::string expected =
    "#########\n"
    "#### ####\n"
    "##     ##\n"
    "##     ##\n"
    "####  ###\n"
    "#########\n";
  StaticDeadTileCheckCornersCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallCoveLevelTest, SinglePushAlgorithm)
{
  std::string expected =
    "#########\n"
    "#########\n"
    "##     ##\n"
    "##     ##\n"
    "####  ###\n"
    "#########\n";
  StaticDeadTileSinglePushCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotSmallCoveLevelTest, InspectAllPushesAlgorithm)
{
  std::string expected =
    "#########\n"
    "#########\n"
    "##     ##\n"
    "##     ##\n"
    "####  ###\n"
    "#########\n";
  StaticDeadTileInspectAllPushesCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

class StaticDeadSpotDeadEndTest : public StaticDeadSpotBaseTest
{
protected:
  static const std::string LvlString;
  StaticDeadSpotDeadEndTest() : StaticDeadSpotBaseTest(LvlString) {}
};
const std::string StaticDeadSpotDeadEndTest::LvlString =
  "      ####\n"
  "#######  #\n"
  "#        #\n"
  "#######  #\n"
  "# @ $    #\n"
  "#      . #\n"
  "##########\n";

TEST_F(StaticDeadSpotDeadEndTest, NoOpAlgorithm)
{
  std::string expected =
    "##########\n"
    "#######  #\n"
    "#        #\n"
    "#######  #\n"
    "#        #\n"
    "#        #\n"
    "##########\n";
  StaticDeadTileNoOpCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotDeadEndTest, CheckCornersAlgorithm)
{
  std::string expected =
    "##########\n"
    "##########\n"
    "##       #\n"
    "#######  #\n"
    "##       #\n"
    "##      ##\n"
    "##########\n";
  StaticDeadTileCheckCornersCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotDeadEndTest, SinglePushAlgorithm)
{
  std::string expected =
    "##########\n"
    "##########\n"
    "##      ##\n"
    "####### ##\n"
    "##      ##\n"
    "##      ##\n"
    "##########\n";
  StaticDeadTileSinglePushCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}

TEST_F(StaticDeadSpotDeadEndTest, InspectAllPushesAlgorithm)
{
  std::string expected =
    "##########\n"
    "##########\n"
    "####### ##\n"
    "####### ##\n"
    "##      ##\n"
    "##      ##\n"
    "##########\n";
  StaticDeadTileInspectAllPushesCheckingAlgorithm algo;
  algo.markDeadTiles(Lvl, Actual);
  EXPECT_TRUE(checkStaticDeadSpots(Actual, expected));
}
