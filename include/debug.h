/* SPDX-License-Identifier: MIT */
#ifndef _DEBUG_H_INCLUDED
#define _DEBUG_H_INCLUDED

#ifndef ENABLE_ASSERTIONS
#define ENBALE_ASSERTIONS 0
#endif

#ifndef ENABLE_GUARANTEES
#define ENABLE_GUARANTEES 1
#endif

#if ENABLE_ASSERTIONS == 0
#define ASSERT(p, ...)
#else
#define ASSERT(p, ...)                                                                  \
  do {                                                                                  \
    if (!(p)) {                                                                         \
      assertion_error(__FILE__, __LINE__, __PRETTY_FUNCTION__, "ASSERT(" #p ") failed", \
                      __VA_ARGS__);                                                     \
    }                                                                                   \
  } while (0)
#endif

#if ENABLE_GUARANTEES == 0
#define GUARANTEE(p, ...)
#else
#define GUARANTEE(p, ...)                                                                  \
  do {                                                                                     \
    if (!(p)) {                                                                            \
      assertion_error(__FILE__, __LINE__, __PRETTY_FUNCTION__, "GUARANTEE(" #p ") failed", \
                      __VA_ARGS__);                                                        \
    }                                                                                      \
  } while (0)
#endif

#define UNREACHABLE()                                                        \
  do {                                                                       \
    assertion_error(__FILE__, __LINE__, __PRETTY_FUNCTION__, "UNREACHABLE"); \
  } while (0)

#define UNIMPLEMENTED()                                                        \
  do {                                                                         \
    assertion_error(__FILE__, __LINE__, __PRETTY_FUNCTION__, "UNIMPLEMENTED"); \
  } while (0)

void assertion_error(const char* file, int line, const char* function, const char* message,
                     const char* details, ...)
__attribute__ ((noreturn));

void assertion_error(const char* file, int line, const char* function, const char* message)
  __attribute__ ((noreturn));

#endif
