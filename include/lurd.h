/* SPDX-License-Identifier: MIT */
#ifndef _LURD_H_INCLUDED
#define _LURD_H_INCLUDED

#include "debug.h"
#include "level.h"
#include <cstddef>
#include <initializer_list>
#include <optional>
#include <vector>
#include <stdexcept>

class LURD
{
public:
  static const char MOVE_LEFT_CHAR = 'l';
  static const char MOVE_UP_CHAR = 'u';
  static const char MOVE_RIGHT_CHAR = 'r';
  static const char MOVE_DOWN_CHAR = 'd';
  static const char PUSH_LEFT_CHAR = 'L';
  static const char PUSH_UP_CHAR = 'U';
  static const char PUSH_RIGHT_CHAR = 'R';
  static const char PUSH_DOWN_CHAR = 'D';

private:
  enum class Action : char
  {
    MoveLeft = MOVE_LEFT_CHAR,
    MoveUp = MOVE_UP_CHAR,
    MoveRight = MOVE_RIGHT_CHAR,
    MoveDown = MOVE_DOWN_CHAR,
    PushLeft = PUSH_LEFT_CHAR,
    PushUp = PUSH_UP_CHAR,
    PushRight = PUSH_RIGHT_CHAR,
    PushDown = PUSH_DOWN_CHAR,
  };

public:
  typedef Action value_type;

  typedef std::vector<Action>::size_type size_type;

  typedef std::vector<Action>::reference reference;
  typedef std::vector<Action>::const_reference const_reference;

  typedef std::vector<Action>::pointer pointer;
  typedef std::vector<Action>::const_pointer const_pointer;
  
  typedef std::vector<Action>::iterator iterator;
  typedef std::vector<Action>::const_iterator const_iterator;

private:
  std::vector<Action> Actions;

  LURD()
    : Actions()
  {
  }

  explicit LURD(std::vector<Action> actions)
    : Actions(actions)
  {
  }

public:

  const_reference at(size_type index) const
  {
    return Actions.at(index);
  }

  const_reference operator[](size_type index) const
  {
    ASSERT(index < Actions.size(), "index out of range");
    return Actions[index];
  }

  const_reference front() const { return Actions.front(); }

  const_reference back() const { return Actions.back(); }

  const_pointer data() const noexcept { return Actions.data(); }

  bool empty() const noexcept { return Actions.empty(); }
  size_type size() const noexcept { return Actions.size(); }
  size_type max_size() const noexcept { return Actions.max_size(); }

  const_iterator begin() const { return Actions.begin(); }
  const_iterator end() const { return Actions.end(); }
  const_iterator cbegin() const { return Actions.cbegin(); }
  const_iterator cend() const { return Actions.cend(); }

  std::string toString()
  {
    std::string result(Actions.size(), '\0');
    for (size_type i = 0; i < Actions.size(); ++i) {
      result[i] = static_cast<char>(Actions[i]);
    }
    return result;
  }

  size_type numberOfMoves() const;
  size_type numberOfPushes() const;

  void visualizeLevel(const Level&) const;

public:

  static LURD fromLURDString(const std::string&);
  static LURD fromStates(const Level&, const std::vector<State>&);

private:

  static std::optional<std::pair<LPos, LPos>> findCrateThatMoved(const State&, const State&);
  static std::optional<std::pair<LPos, LPos>> findPlayerPositionsForTransition(const Level&, const std::pair<LPos, LPos>&);
  static Action calculateActionForTransition(const Level&, const std::pair<LPos, LPos>&);

  static std::vector<LPos> findShortestPlayerPath(const Level&, const State&, const LPos&, const LPos&);
  static std::vector<Action> pathToActions(const Level&, const std::vector<LPos>&);

};

#endif
