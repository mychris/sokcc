/* SPDX-License-Identifier: MIT */
#ifndef _DEAD_SPOTS_H_INCLUDED
#define _DEAD_SPOTS_H_INCLUDED

#include "carray.h"
#include "level.h"
#include "tile_marking.h"
#include <memory>
#include <vector>

class DeadTileChecker
{
public:
  virtual ~DeadTileChecker() = default;
  virtual void configureFor(const Level&) = 0;
  virtual bool isCrateOnDeadTile(const Level&, const LPos&) = 0;
};

class StaticDeadTileChecker : public DeadTileChecker
{
protected:
  TileMarkings StaticDeadTiles;

  StaticDeadTileChecker()
    : StaticDeadTiles()
  {
  }

  virtual void markDeadTiles(const Level&, TileMarkings&) = 0;

public:
  void configureFor(const Level& level) final override
  {
    StaticDeadTiles = TileMarkings(level);
    markDeadTiles(level, StaticDeadTiles);
  }

  bool isCrateOnDeadTile(const Level& level, const LPos& cratePos) final override
  {
    (void) level;
    ASSERT(level.length() == StaticDeadTiles.numberOfTiles(), "check");
    return StaticDeadTiles.isTileMarked(cratePos);
  }

};

class StaticDeadTileNoOpCheckingAlgorithm : public StaticDeadTileChecker
{
public:
  void markDeadTiles(const Level&, TileMarkings&) override;
};

class StaticDeadTileCheckCornersCheckingAlgorithm : public StaticDeadTileChecker
{
public:
  void markDeadTiles(const Level&, TileMarkings&) override;
};

class StaticDeadTileSinglePushCheckingAlgorithm : public StaticDeadTileChecker
{
public:
  void markDeadTiles(const Level&, TileMarkings&) override;
};

class StaticDeadTileInspectAllPushesCheckingAlgorithm : public StaticDeadTileChecker
{
private:
  enum Mark {
    Bad = 0,
    Good = 1,
    Unmarked = 2,
  };
  struct States {
    LData<bool> CrateStates;
    LData<std::vector<LPos>> PlayerPositions;

    explicit States(const Level& level)
      : CrateStates(level.length())
      , PlayerPositions(level.length())
    {
    }

    ~States()
    {
    }

    States(const States&) = delete;
    States(States&&) = delete;
    States& operator=(const States&) = delete;
    States& operator=(States&&) = delete;

    bool addPlayerPosition(const LPos&, const LPos&);
    bool playerPositionAvailable(const LPos&, const LPos&) const;
  };

  void findAllPossibleStates(States&, const Level&) const;
  void markUnreachableStates(const States&, const Level&, TileMarkings&) const;
  LPos normalizePlayerPosition(const Level&, const LPos&, const LPos&) const;

public:
  void markDeadTiles(const Level&, TileMarkings&) override;
};

class DynamicDeadTileChecker : public DeadTileChecker
{
public:
  virtual ~DynamicDeadTileChecker() = default;
  virtual void configureFor(const Level&) = 0;
  virtual bool isCrateOnDeadTile(const Level&, const LPos&) = 0;
};

class DynamicDeadTileNoOpCheckingAlgorithm final : public DynamicDeadTileChecker
{
public:
  void configureFor(const Level&)override;
  bool isCrateOnDeadTile(const Level&, const LPos&) override;
};

class DynamicDeadTileTestNeighboursCheckingAlgorithm final : public DynamicDeadTileChecker
{
private:
  enum CrateState {
    NoCrate,
    Unknown,
    InProgress,
    Blocked,
    NotBlocked,
  };
  
  TileMarkings StaticDeadSpots;
  LData<CrateState> CrateStates;

public:
  DynamicDeadTileTestNeighboursCheckingAlgorithm()
    : StaticDeadSpots()
    , CrateStates()
  {
  }
  
  ~DynamicDeadTileTestNeighboursCheckingAlgorithm()
  {
  }
  
  void configureFor(const Level&) override;
  bool isCrateOnDeadTile(const Level&, const LPos&) override;

private:
  bool isCrateBlocked(const Level&, const LPos&);
  bool isPlaneBlocked(const Level&, const LPos&, const LPos&);
};

#endif
