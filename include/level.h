/*SPDX-License-Identifier: MIT */
#ifndef _LEVEL_H_INCLUDED
#define _LEVEL_H_INCLUDED

#include "debug.h"
#include "types.h"
#include <algorithm>
#include <optional>
#include <string>
#include <utility>
#include <vector>

class State final
{
private:
  LPos PlayerPosition;
  std::size_t NumberOfCrates;
  LPos* CratePositions;

public:
  State(LPos playerPosition, std::size_t numberOfCrates, LPos* cratePositions)
    : PlayerPosition(playerPosition)
    , NumberOfCrates(numberOfCrates)
    , CratePositions(cratePositions)
  {
  }

  State(const State& o) : State(o, nullptr)
  {
  }

  State(const State& o, LPos* cratePositions)
    : PlayerPosition(o.PlayerPosition)
    , NumberOfCrates(o.NumberOfCrates)
    , CratePositions((cratePositions) ? cratePositions : new LPos[o.NumberOfCrates])
  {
    if (!cratePositions) {
      std::copy(o.CratePositions, o.CratePositions + o.NumberOfCrates, CratePositions);
    }
  }

  State(State&& o) noexcept
    : PlayerPosition(o.PlayerPosition)
    , NumberOfCrates(o.NumberOfCrates)
    , CratePositions(std::exchange(o.CratePositions, nullptr))
  {
  }

  ~State()
  {
    delete[] CratePositions;
  }

  State& operator=(const State&);

  State& operator=(State&&) noexcept;

  const LPos& playerPosition() const noexcept
  {
    return PlayerPosition;
  }

  std::size_t numberOfCrates() const noexcept
  {
    return NumberOfCrates;
  }

  const LPos& operator[](std::size_t pos) const
  {
    ASSERT(pos < NumberOfCrates, "crate index %zu out of range", pos);
    return CratePositions[pos];
  }

  State movePlayer(const LPos position) const
  {
    State s = *this;
    s.PlayerPosition = position;
    return s;
  }

  State moveCrate(std::size_t crate, const LPos& position) const
  {
    State s = *this;
    s.CratePositions[crate] = position;
    std::sort(s.CratePositions, s.CratePositions + s.NumberOfCrates);
    return s;
  }

  State movePlayerAndCrate(const LPos& playerPosition, std::size_t crate, const LPos& cratePosition) const
  {
    State s = *this;
    s.PlayerPosition = playerPosition;
    s.CratePositions[crate] = cratePosition;
    std::sort(s.CratePositions, s.CratePositions + s.NumberOfCrates);
    return s;
  }

  bool doesCrateOccupy(const LPos& position) const
  {
    for (std::size_t i = 0; i < NumberOfCrates; ++i) {
      if (CratePositions[i] == position) {
        return true;
      }
    }
    return false;
  }

  const LPos& crateAt(std::size_t index) const
  {
    ASSERT(index < NumberOfCrates, "crate index %zu out of range", index);
    return CratePositions[index];
  }

  bool operator==(const State&) const;
  bool operator!=(const State&) const;
};

class LevelValidationResult final
{
private:
  bool Result;
  std::string Message;

  LevelValidationResult(bool result, const std::string& message)
    : Result(result)
    , Message(message)
  {
  }

  LevelValidationResult(bool result, std::string&& message)
    : Result(result)
    , Message(std::move(message))
  {
  }

public:
  bool isGood() const
  {
    return Result;
  }

  const std::string& getMessage() const
  {
    return Message;
  }

  static LevelValidationResult good()
  {
    return LevelValidationResult(true, "");
  }

  static LevelValidationResult bad(const std::string& message)
  {
    return LevelValidationResult(false, message);
  }

  static LevelValidationResult bad(std::string&& message)
  {
    return LevelValidationResult(false, std::move(message));
  }
};

class Level final
{

public:
  static const char FLOOR_CHAR = ' ';
  static const char WALL_CHAR = '#';
  static const char CRATE_CHAR = '$';
  static const char SPOT_CHAR = '.';
  static const char CRATE_ON_SPOT_CHAR = '*';
  static const char PLAYER_CHAR = '@';
  static const char PLAYER_ON_SPOT_CHAR = '+';

private:
  std::size_t Width;
  std::size_t Height;
  std::size_t Length;
  LPos PlayerPosition;
  std::size_t NumberOfCrates;
  LPos* CratePositions;
  LData<bool> ReachablePositions;
  LPos* ReachablePositionsQueue;
  bool ReachablePositionsValid;
  LData<uint16_t> ShortestPathCache;
  LPos* ShortestPathQueue;
  LData<LElement> Data;

public:
  Level();

  Level(std::size_t, std::size_t, LElement*);

  ~Level();

  Level(const Level&);

  Level(Level&&) noexcept;

  Level &operator=(const Level&);

  Level &operator=(Level&&) noexcept;

  std::size_t width() const noexcept { return Width; }

  std::size_t height() const noexcept { return Height; }

  std::size_t length() const noexcept { return Length; }

  LPosRange positions() const noexcept
  {
    return LPosRange(LPos(0, Length), Length);
  }

  LElement operator[](const LPos& position) const
  {
    ASSERT(position < Length, "index out of range: %zu", static_cast<std::size_t>(position));
    return Data[position];
  }

  bool isWallAt(const LPos& position) const
  {
    bool result = (Data[position] & LElement::Wall) != 0;
    ASSERT(!result || Data[position] == LElement::Wall, "invariant: %d", Data[position]);
    return result;
  }

  bool isCrateAt(const LPos& position) const
  {
    bool result = (Data[position] & LElement::Crate) != 0;
    ASSERT(!result || (Data[position] & ~(LElement::Crate | LElement::Spot | LElement::Floor)) == 0, "invariant: %d", Data[position]);
    ASSERT(!result || (Data[position] & LElement::Floor) != 0, "invariant: %d", Data[position]);
    return result;
  }

  bool isSpotAt(const LPos& position) const
  {
    bool result = (Data[position] & LElement::Spot) != 0;
    ASSERT(!result || (Data[position] & LElement::Wall) == 0, "invariant: %d", Data[position]);
    ASSERT(!result || (Data[position] & LElement::Floor) != 0, "invariant: %d", Data[position]);
    return result;
  }

  bool isPlayerAt(const LPos& position) const
  {
    bool result = (Data[position] & LElement::Player) != 0;
    ASSERT(!result || position == PlayerPosition, "invariant");
    ASSERT(!result || (Data[position] & LElement::Floor) != 0, "invariant: %d", Data[position]);
    return result;
  }

  const LPos& playerPosition() const noexcept { return PlayerPosition; }

  std::size_t numberOfCrates() const noexcept { return NumberOfCrates; }

  const LPos& crateAt(std::size_t idx) const
  {
    ASSERT(idx < NumberOfCrates, "index out of range: %zu", idx);
    return CratePositions[idx];
  }

  void normalizePlayerPosition();

  void movePlayerTo(const LPos&);

  void moveCrateTo(std::size_t, const LPos&);

  void moveCrateFromTo(const LPos&, const LPos&);

  void moveCrateAndPlayerFromTo(const LPos&, const LPos&, const LPos&);

  void swapCrateAndPlayer(const LPos&, const LPos&);

  void removeCrate(std::size_t);

  void addSpot(const LPos&);

  const LData<bool>& reachablePositions();
  const LData<bool>& reachablePositionsIgnoringCrates();

  State currentState() const;

  void updateToState(const State&);

  bool isSolved() const;

  std::optional<uint16_t> shortestPlayerPathTo(const LPos&);

  void visualize() const;

  static Level parseFromString(const std::string&);

  static Level parseFromLines(const std::vector<std::string>&);

  LevelValidationResult verifyAndNormalizeLevel();
};

#endif
