/* SPDX-License-Identifier: MIT */
#ifndef _TYPES_H_INCLUDED
#define _TYPES_H_INCLUDED

#include "debug.h"
#include <cstddef>
#include <cstring>
#include <limits>
#include <ostream>
#include <utility>
#include <vector>

class LPos final
{
private:
  size_t Position;
  size_t LevelSize;

public:
  LPos() noexcept
    : Position(0)
    , LevelSize(0)
  {
  }

  LPos(const size_t& position, const size_t& levelSize)
    : Position(position)
    , LevelSize(levelSize)
  {
    // TODO: For now, LevelSize is only used in debug builds with assertions enabled.
    (void) LevelSize;
    ASSERT((position == 0 && levelSize == 0) || position < levelSize,
           "Invalid position %zu for level with size %zu", position, LevelSize);
  }

  LPos(const LPos&) noexcept = default;
  LPos(LPos&&) noexcept = default;

  LPos& operator=(const size_t& position)
  {
    ASSERT((position == 0 && LevelSize == 0) || position < LevelSize,
           "position %zu would overflow level size %zu", position, LevelSize);
    Position = position;
    return *this;
  }

  LPos& operator=(const int& position)
  {
    ASSERT(position >= 0, "negative position %d", position);
    return operator=(static_cast<size_t>(position));
  }

  LPos& operator=(const LPos&) noexcept = default;
  LPos& operator=(LPos&&) noexcept = default;

  size_t get() const noexcept
  {
    return Position;
  }

  int difference(const LPos& other) const noexcept
  {
    ASSERT(LevelSize == other.LevelSize, "invariant");
    return static_cast<int>(Position) - static_cast<int>(other.Position);
  }

  explicit operator size_t() const noexcept
  {
    return Position;
  }

  explicit operator bool() const noexcept
  {
    return Position > 0;
  }

  LPos& operator++()
  {
    ASSERT(Position < LevelSize - 1, "overflow");
    Position++;
    return *this;
  }

  LPos operator++(int)
  {
    LPos old(*this);
    operator++();
    return old;
  }

  LPos& operator--()
  {
    ASSERT(Position > 0, "overflow");
    Position--;
    return *this;
  }

  LPos operator--(int)
  {
    LPos old(*this);
    operator--();
    return old;
  }

  LPos& operator+=(const size_t& rhs)
  {
    ASSERT(LevelSize - Position > rhs,
           "overflow: %zu + %zu >= %zu", Position, rhs, LevelSize);
    Position += rhs;
    return *this;
  }

  friend LPos operator+(LPos lhs, const size_t& rhs)
  {
    lhs += rhs;
    return lhs;
  }

  LPos& operator+=(const int& rhs)
  {
    ASSERT(rhs != std::numeric_limits<int>::min(),
           "out of range: %d", rhs);
    if (rhs < 0) {
      return operator-=(static_cast<size_t>(-rhs));
    } else {
      return operator+=(static_cast<size_t>(rhs));
    }
  }

  friend LPos operator+(LPos lhs, const int& rhs)
  {
    lhs += rhs;
    return lhs;
  }

  LPos& operator+=(const LPos& rhs)
  {
    ASSERT(LevelSize == rhs.LevelSize, "invariant");
    return operator+=(rhs.Position);
  }

  friend LPos operator+(LPos lhs, const LPos& rhs)
  {
    lhs += rhs;
    return lhs;
  }

  LPos& operator-=(const size_t& rhs)
  {
    ASSERT(Position >= rhs, "overflow");
    Position -= rhs;
    return *this;
  }

  friend LPos operator-(LPos lhs, const size_t& rhs)
  {
    lhs -= rhs;
    return lhs;
  }

  LPos& operator-=(const int& rhs)
  {
    ASSERT(rhs != std::numeric_limits<int>::min(),
           "out of range: %d", rhs);
    if (rhs < 0) {
      return operator+=(static_cast<size_t>(-rhs));
    } else {
      return operator-=(static_cast<size_t>(rhs));
    }
  }

  friend LPos operator-(LPos lhs, const int& rhs)
  {
    lhs -= rhs;
    return lhs;
  }

  LPos& operator-=(const LPos& rhs)
  {
    return operator-=(rhs.Position);
  }

  friend LPos operator-(LPos lhs, const LPos& rhs)
  {
    lhs -= rhs;
    return lhs;
  }

  bool operator==(const LPos& rhs) const
  {
    ASSERT(LevelSize == rhs.LevelSize,
           "invariant: %zu != %zu", LevelSize, rhs.LevelSize);
    return Position == rhs.Position;
  }

  friend bool operator==(const size_t& lhs, const LPos& rhs) { return lhs == rhs.Position; }
  friend bool operator==(const LPos& lhs, const size_t& rhs) { return lhs.Position == rhs; }
  friend bool operator==(const int& lhs, const LPos& rhs) { return lhs >= 0 && static_cast<size_t>(lhs) == rhs; }
  friend bool operator==(const LPos& lhs, const int& rhs) { return rhs >= 0 && lhs == static_cast<size_t>(rhs); }

  friend bool operator!=(const LPos& lhs, const LPos& rhs) { return !(lhs == rhs); }
  friend bool operator!=(const size_t& lhs, const LPos& rhs) { return !(lhs == rhs); }
  friend bool operator!=(const LPos& lhs, const size_t& rhs) { return !(lhs == rhs); }
  friend bool operator!=(const int& lhs, const LPos& rhs) { return !(lhs == rhs); }
  friend bool operator!=(const LPos& lhs, const int& rhs) { return !(lhs == rhs); }

  bool operator<(const LPos& rhs) const
  {
    ASSERT(LevelSize == rhs.LevelSize,
           "invariant: %zu != %zu", LevelSize, rhs.LevelSize);
    return Position < rhs.Position;
  }

  friend bool operator<(const size_t& lhs, const LPos& rhs)
  {
    return lhs < rhs.Position;
  }

  friend bool operator<(const LPos& lhs, const size_t& rhs)
  {
    return lhs.Position < rhs;
  }

  friend bool operator<(const int& lhs, const LPos& rhs)
  {
    return lhs < 0 || static_cast<size_t>(lhs) < rhs;
  }

  friend bool operator<(const LPos& lhs, const int& rhs)
  {
    return rhs >= 0 && lhs < static_cast<size_t>(rhs);
  }

  bool operator<=(const LPos& rhs) const
  {
    ASSERT(LevelSize == rhs.LevelSize,
           "invariant: %zu != %zu", LevelSize, rhs.LevelSize);
    return Position <= rhs.Position;
  }

  friend bool operator<=(const size_t& lhs, const LPos& rhs)
  {
    return lhs <= rhs.Position;
  }

  friend bool operator<=(const LPos& lhs, const size_t& rhs)
  {
    return lhs.Position <= rhs;
  }

  friend bool operator<=(const int& lhs, const LPos& rhs)
  {
    return lhs <= 0 || static_cast<size_t>(lhs) <= rhs;
  }

  friend bool operator<=(const LPos& lhs, const int& rhs)
  {
    return rhs >= 0 && lhs <= static_cast<size_t>(rhs);
  }

  bool operator>(const LPos& rhs) const
  {
    ASSERT(LevelSize == rhs.LevelSize,
           "invariant: %zu != %zu", LevelSize, rhs.LevelSize);
    return Position > rhs.Position;
  }

  friend bool operator>(const size_t& lhs, const LPos& rhs)
  {
    return lhs > rhs.Position;
  }

  friend bool operator>(const LPos& lhs, const size_t& rhs)
  {
    return lhs.Position > rhs;
  }

  friend bool operator>(const int& lhs, const LPos& rhs)
  {
    return lhs > 0 && static_cast<size_t>(lhs) > rhs;
  }

  friend bool operator>(const LPos& lhs, const int& rhs)
  {
    return rhs < 0 || lhs > static_cast<size_t>(rhs);
  }

  bool operator>=(const LPos& rhs) const
  {
    ASSERT(LevelSize == rhs.LevelSize,
          "invariant: %zu != %zu", LevelSize, rhs.LevelSize);
    return Position >= rhs.Position;
  }

  friend bool operator>=(const size_t& lhs, const LPos& rhs)
  {
    return lhs >= rhs.Position;
  }

  friend bool operator>=(const LPos& lhs, const size_t& rhs)
  {
    return lhs.Position >= rhs;
  }

  friend bool operator>=(const int& lhs, const LPos& rhs)
  {
    return lhs >= 0 && static_cast<size_t>(lhs) >= rhs;
  }

  friend bool operator>=(const LPos& lhs, const int& rhs)
  {
    return rhs < 0 || lhs >= static_cast<size_t>(rhs);
  }

  friend std::ostream& operator<<(std::ostream& os, const LPos& position) {
    return os << "LPos(" << position.Position << ", " << position.LevelSize << ")";
  }

};

enum LElement {
  Floor = 1 << 0,
  Wall = 1 << 1,
  Crate = 1 << 2,
  Spot = 1 << 3,
  Player = 1 << 4,
};

template<class T>
class LData final
{
private:
  std::size_t Length;
  T* Data;

public:
  LData()
    : Length(0)
    , Data(nullptr)
  {
  }

  explicit LData(std::size_t length)
    : Length(length)
    , Data((length == 0) ? nullptr : new T[length]())
  {
  }

  LData(T *data, std::size_t length)
    : Length(length)
    , Data(data)
  {
  }

  LData(const LData<T>& other)
    : Length(other.Length)
    , Data((other.Length == 0) ? nullptr : new T[other.Length])
  {
    if (other.Length > 0) {
      if constexpr (std::is_trivial<T>::value) {
        std::memcpy(Data, other.Data, sizeof(T) * other.Length);
      } else {
        std::copy(other.begin(), other.end(), Data);
      }
    }
  }

  LData(LData<T>&& other) noexcept
    : Length(std::exchange(other.Length, 0))
    , Data(std::exchange(other.Data, nullptr))
  {
  }

  ~LData()
  {
    delete[] Data;
  }

  LData<T>& operator=(const LData<T>& other)
  {
    delete[] Data;
    Data = (other.Length == 0) ? nullptr : new T[other.Length];
    Length = other.Length;
    if (other.Length > 0) {
      if constexpr (std::is_trivial<T>::value) {
        std::memcpy(Data, other.Data, sizeof(T) * other.Length);
      } else {
        std::copy(other.begin(), other.end(), Data);
      }
    }
    return *this;
  }

  LData<T>& operator=(LData<T>&& other) noexcept
  {
    std::swap(Data, other.Data);
    std::swap(Length, other.Length);
    return *this;
  }

  const T& operator[](const LPos& position) const
  {
    ASSERT(static_cast<std::size_t>(position) < Length, "index %zu out of bounds for LData of length %zu", static_cast<std::size_t>(position), Length);
    return Data[static_cast<std::size_t>(position)];
  }

  T& operator[](const LPos& position)
  {
    ASSERT(static_cast<std::size_t>(position) < Length, "index %zu out of bounds for LData of length %zu", static_cast<std::size_t>(position), Length);
    return Data[static_cast<std::size_t>(position)];
  }

  std::size_t length() const noexcept
  {
    return Length;
  }

  T* data() noexcept
  {
    return Data;
  }

  const T* data() const noexcept
  {
    return Data;
  }

  T* begin() noexcept
  {
    return Data;
  }

  const T* begin() const noexcept
  {
    return Data;
  }

  T* end() noexcept
  {
    return Data + Length;
  }

  const T* end() const noexcept
  {
    return Data + Length;
  }
};

class LPosRange final
{
private:
  std::size_t CurrentValue;
  std::size_t EndValue;
  LPos Current;

  bool isAtEnd() const noexcept {
    return CurrentValue >= EndValue;
  }

public:
  class Iterator final
  {
  private:
    LPosRange* Range;

    class PostIncReturn final
    {
    private:
      LPos Value;
    public:
      PostIncReturn(LPos value)
        : Value(value)
      {
      }

      LPos& operator*() noexcept
      {
        return Value;
      }

      const LPos& operator*() const noexcept
      {
        return Value;
      }
    };

  public:
    using value_type = LPos;
    using reference = value_type&;
    using iterator_catergory = std::input_iterator_tag;
    using pointer = value_type*;
    using difference_type = void;

    explicit Iterator(LPosRange* const range)
      : Range(range)
    {
      if (range && range->isAtEnd()) {
        Range = nullptr;
      }
    }

    const LPos& operator*() const
    {
      ASSERT(Range, "accessing past-the-end iterator");
      return Range->Current;
    }

    const LPos* operator->() const
    {
      ASSERT(Range, "accessing past-the-end iterator");
      return &Range->Current;
    }

    Iterator& operator++()
    {
      ASSERT(Range, "iterating past the end");
      Range->CurrentValue += 1;
      if (Range->isAtEnd()) {
        Range = nullptr;
      } else {
        ++(Range->Current);
      }
      return *this;
    }

    PostIncReturn operator++(int)
    {
      PostIncReturn tmp(**this);
      ++(*this);
      return tmp;
    }

    friend bool operator==(const Iterator& lhs, const Iterator& rhs)
    {
      return lhs.Range == rhs.Range;
    }

    friend bool operator!=(const Iterator& lhs, const Iterator& rhs)
    {
      return !(lhs == rhs);
    }
  };

  LPosRange(const LPos& start, const std::size_t steps)
    : CurrentValue(static_cast<std::size_t>(start))
    , EndValue(static_cast<std::size_t>(start) + steps)
    , Current(start)
  {
    if (steps > 0) {
      ASSERT(Current + (steps - 1) >= 0, "too many steps");
    }
  }

  LPosRange(LPos&& start, const std::size_t steps)
    : CurrentValue(static_cast<std::size_t>(start))
    , EndValue(static_cast<std::size_t>(start) + steps)
    , Current(std::move(start))
  {
    if (steps > 0) {
      ASSERT(Current + (steps - 1) >= 0, "too many steps");
    }
  }

  Iterator begin()
  {
    return Iterator(this);
  }

  Iterator end()
  {
    return Iterator(nullptr);
  }
  
};

#endif
