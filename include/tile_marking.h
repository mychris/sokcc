/* SPDX-License-Identifier: MIT */
#ifndef _TILE_MARKING_H_INCLUDED
#define _TILE_MARKING_H_INCLUDED

#include "debug.h"
#include "level.h"
#include "types.h"
#include <cstddef>
#include <utility>

class TileMarkings final
{
private:
  std::size_t Length;
  LData<bool> Data;

public:
  TileMarkings()
    : Length(0)
    , Data()
  {
  }

  explicit TileMarkings(const Level& level)
    : Length(level.length())
    , Data(level.length())
  {
    std::fill(Data.begin(), Data.end(), false);
  }

  TileMarkings(const TileMarkings& o) = default;

  TileMarkings(TileMarkings&& o) noexcept = default;

  ~TileMarkings() = default;

  TileMarkings& operator=(const TileMarkings&) = default;

  TileMarkings& operator=(TileMarkings&&) noexcept = default;

  void markTile(const LPos& position)
  {
    ASSERT(position < Length, "position %zu out of range", position);
    Data[position] = true;
  }

  void markAllTiles()
  {
    std::fill(Data.begin(), Data.end(), true);
  }

  void unmarkTile(const LPos& position) 
  {
    ASSERT(position < Length, "position %zu out of range", position);
    Data[position] = false;
  }

  void unmarkAllTiles()
  {
    std::fill(Data.begin(), Data.end(), false);
  }

  bool flipTile(const LPos& position)
  {
    ASSERT(position < Length, "position %zu out of range", position);
    bool result = Data[position];
    Data[position] = !result;
    return result;
  }

  bool isTileMarked(const LPos& position) const
  {
    ASSERT(position < Length, "position %zu out of range", position);
    return Data[position];
  }

  std::size_t numberOfTiles() const noexcept
  {
    return Length;
  }

  std::size_t numberOfMarkedTiles() const noexcept;

  void merge(const TileMarkings&);

  TileMarkings join(const TileMarkings&) const;

  bool operator==(const TileMarkings& o) const noexcept
  {
    if (Length != o.Length) {
      return false;
    }
    for (const auto& pos : LPosRange(LPos(0, Length), Length)) {
      if (Data[pos] != o.Data[pos]) {
        return false;
      }
    }
    return true;
  }

  bool operator!=(const TileMarkings& o) const noexcept
  {
    return !(*this == o);
  }

};

#endif
