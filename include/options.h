/* SPDX-License-Identifier: MIT */
#ifndef _OPTIONS_H_INCLUDED
#define _OPTIONS_H_INCLUDED

#include <string>

struct Opts
{
};

extern Opts Options;
extern std::string ProgramName;

void parseOptionsFromCommandLine(int argc, char** argv);

#endif
