/* SPDX-License-Identifier: MIT */
#ifndef _SOLVER_H_INCLUDED
#define _SOLVER_H_INCLUDED

#include "level.h"
#include "lurd.h"
#include "dead_spot.h"
#include "state_table.h"
#include <memory>
#include <optional>
#include <string>
#include <future>
#include <queue>

class Solver
{
public:
  class Builder;

  virtual ~Solver() = default;
  virtual LURD solveLevel(const Level&) = 0;
};

class Solver::Builder final
{
private:
  std::unique_ptr<StaticDeadTileChecker> SDTMA;
  std::unique_ptr<DynamicDeadTileChecker> DDTCA;

public:
  Builder()
    : SDTMA(std::make_unique<StaticDeadTileNoOpCheckingAlgorithm>())
    , DDTCA(std::make_unique<DynamicDeadTileNoOpCheckingAlgorithm>())
  {
  }

  Builder& usingStaticDeadSpotDetection(std::unique_ptr<StaticDeadTileChecker>&& algo)
  {
    SDTMA = std::move(algo);
    return *this;
  }

  Builder& usingDynamicDeadSpotChecker(std::unique_ptr<DynamicDeadTileChecker>&& algo)
  {
    DDTCA = std::move(algo);
    return *this;
  }

  template <class Q>
  std::unique_ptr<Solver> build()
  {
    auto result = std::make_unique<Q>(std::move(SDTMA), std::move(DDTCA));
    SDTMA = std::make_unique<StaticDeadTileNoOpCheckingAlgorithm>();
    DDTCA = std::make_unique<DynamicDeadTileNoOpCheckingAlgorithm>();
    return result;
  }
};

class AsyncSolver final
{
private:
  std::unique_ptr<Solver> RealSolver;
  std::future<LURD> LastFuture;

public:
  AsyncSolver(std::unique_ptr<Solver> realSolver)
    : RealSolver(std::move(realSolver))
  {
  }

  ~AsyncSolver()
  {
    if (LastFuture.valid()) {
      LastFuture.wait();
    }
  }

  void solveLevelAsync(const Level&);

  bool solutionComputed() const;

  LURD computedSoltion();
};

class SimpleBreadthFirstSolver final : public Solver
{
private:
  Level OriginalLvl;
  std::unique_ptr<StateTable> Table;

  std::unique_ptr<StaticDeadTileChecker> StaticDeadSpotChecker;
  std::unique_ptr<DynamicDeadTileChecker> DynamicDeadSpotChecker;
  std::queue<StateTableEntry*> Queue;
  size_t BestSolutionMoves;
  size_t BestSolutionPushes;

public:
  explicit SimpleBreadthFirstSolver(std::unique_ptr<StaticDeadTileChecker>,
                                    std::unique_ptr<DynamicDeadTileChecker>);

  ~SimpleBreadthFirstSolver();

  LURD solveLevel(const Level&) override;

private:

  LURD constructLurdFromEndState(const State&);

  void handleState(Level&, StateTableEntry*);

  void visualize(const Level&) const;

  void visualize(const Level& level, const TileMarkings& markings) const;
};

#endif
