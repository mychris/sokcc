/* SPDX-License-Identifier: MIT */
#ifndef _CARRAY_H_INCLUDED
#define _CARRAY_H_INCLUDED

#include <cstddef>

#include <algorithm>
#include <stdexcept>

template <typename T>
class CArray final
{

public:
  typedef T value_type;

  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;
  
  typedef pointer iterator;
  typedef const_pointer const_iterator;

private:
  size_type Size;
  pointer Data;

public:
  CArray(size_type size)
    : Size(size)
  {
    Data = static_cast<pointer>(::operator new(sizeof(value_type) * Size));
    const_pointer end = Data + Size;
    for (pointer ptr = Data; ptr != end; ++ptr) {
      new (ptr) value_type();
    }
  }

  template <class It>
  CArray(It first, It last)
    : Size(std::distance(first, last))
  {
    Data = static_cast<pointer>(::operator new(sizeof(value_type) * Size));
    for (pointer ptr = Data; first != last; ++first, ++ptr) {
      new (ptr) value_type(*first);
    }
  }

  CArray(std::initializer_list<value_type> elems)
    : CArray(elems.begin(), elems.end())
  {}

  CArray(const CArray& o)
    : CArray(o.begin(), o.end())
  {}

  CArray(CArray&& o) noexcept
    : Size(std::move(o.Size))
    , Data(std::move(o.Data))
  {
    o.Size = 0;
    o.Data = nullptr;
  }

  CArray& operator=(CArray o) noexcept
  {
    std::swap(Size, o.Size);
    std::swap(Data, o.Data);
    return *this;
  }

  ~CArray()
  {
    for (iterator cur = begin(); cur != end(); ++cur) {
      cur->~value_type();
    }
    ::operator delete(Data);
  }

  reference at(size_type index)
  {
    return Data[index];
  }
  const_reference at(size_type index) const
  {
    return Data[index];
  }

  reference operator[](size_type index) { return Data[index]; }
  const_reference operator[](size_type index) const { return Data[index]; }

  reference font() { return Data[0]; }
  const_reference front() const { return Data[0]; }

  reference back() { return Data[Size - 1]; }
  const_reference back() const { return Data[Size - 1]; }

  pointer data() { return Data; }
  const_pointer data() const { return Data; }

  bool empty() const noexcept { return Size == 0; }
  size_type size() const noexcept { return Size; }
  size_type max_size() const noexcept { return Size; }

  iterator begin() const { return Data; }
  iterator end() const { return Data + Size; }
  const_iterator cbegin() const { return Data; }
  const_iterator cend() const { return Data + Size; }
};

#endif
