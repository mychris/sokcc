/* SPDX-License-Identifier: MIT */
#ifndef _ALGORITHMS_H_INCLUDED
#define _ALGORITHMS_H_INCLUDED

#include "types.h"
#include "level.h"
#include <functional>

inline bool* clone(const bool* from, size_t length)
{
  bool* result = new bool[length];
  std::copy(from, from + length, result);
  return result;
}

void reachablePositions(const size_t width,
                        const size_t height,
                        const LPos& position,
                        const std::function<bool (const LPos&)>& predicate,
                        LData<bool>& result);

#endif
