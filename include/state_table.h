/* SPDX-License-Identifier: MIT */
#ifndef _STATE_TABLE_H_INCLUDED
#define _STATE_TABLE_H_INCLUDED

#include "debug.h"
#include "types.h"
#include "level.h"
#include <cstdint>
#include <memory>
#include <new>
#include <algorithm>
#include <utility>
#include <map>
#include <vector>
#include <iostream>
#include <functional>
#include <random>

class StateTableEntry
{
  friend class StateTableBucket;
  friend class StateTable;

private:
  State Entry;
  std::size_t EntryHash;
  uint16_t Pushes;
  uint16_t Moves;
  StateTableEntry* Predecessor;

public:
  explicit StateTableEntry(const State& entry, std::size_t entryHash)
    : Entry(entry)
    , EntryHash(entryHash)
    , Pushes(UINT16_MAX)
    , Moves(UINT16_MAX)
    , Predecessor(nullptr)
  {
  }

  ~StateTableEntry()
  {
  }

  const State& entry() const { return Entry; }

  std::size_t entryHash() const { return EntryHash; }

  void setPushes(uint16_t p) { Pushes = p; }

  uint16_t numberOfPushes() const { return Pushes; }

  void setMoves(uint16_t m) { Moves = m; }

  uint16_t numberOfMoves() const { return Moves; }

  void setPredecessor(StateTableEntry* pred) { Predecessor = pred; }

  StateTableEntry* predecessor() { return Predecessor; }

  const StateTableEntry* predecessor() const { return Predecessor; }
};

class StateTableBucket
{
  friend class StateTable;

private:
  std::size_t Allocated;
  std::size_t Used;
  StateTableEntry* FirstEntry;
  StateTableBucket* Next;

public:
  StateTableBucket()
    : Allocated(128)
    , Used(0)
    , FirstEntry(static_cast<StateTableEntry*>(malloc(sizeof(StateTableEntry) * 128)))
    , Next(nullptr)
  {
  }

  ~StateTableBucket()
  {
    clear();
    free(FirstEntry);
    FirstEntry = nullptr;
  }

  StateTableBucket(const StateTableBucket& o) = delete;

  StateTableBucket& operator=(const StateTableBucket& o) = delete;

  void clear()
  {
    delete Next;
    Next = nullptr;
    for (std::size_t i = 0; i < Used; ++i) {
      FirstEntry[i].~StateTableEntry();
    }
    Used = 0;
  }

  StateTableEntry* insert(const State& s, std::size_t hash)
  {
    StateTableEntry* entry = find(s, hash);
    if (!entry) {
      return internalInsert(s, hash);
    }
    return entry;
  }

  StateTableEntry* find(const State& s, std::size_t hash)
  {
    for (std::size_t i = 0; i < Used; ++i) {
      if (hash == FirstEntry[i].EntryHash && FirstEntry[i].Entry == s) {
        return &FirstEntry[i];
      }
    }
    return (Next) ? Next->find(s, hash) : nullptr;
  }

private:
  StateTableEntry* internalInsert(const State& s, std::size_t hash)
  {
    if (Allocated == Used) {
      Next = (Next == nullptr) ? new StateTableBucket() : Next;
      return Next->internalInsert(s, hash);
    }
    StateTableEntry* entry = new (FirstEntry + Used) StateTableEntry(s, hash);
    Used += 1;
    return entry;
  }
};

class StateTable
{
private:
  std::size_t NumberOfBuckets;
  StateTableBucket* Buckets;
  LData<size_t> Randoms;

public:
  explicit StateTable(const Level& level)
    : NumberOfBuckets(8192)
    , Buckets(new StateTableBucket[8192])
    , Randoms(level.length())
  {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<std::size_t> distrib(0, SIZE_MAX);
    for (const auto& pos : level.positions()) {
      Randoms[pos] = distrib(gen);
    }
  }

  ~StateTable()
  {
    delete[] Buckets;
  }

  StateTable(const StateTable& o) = delete;

  StateTable& operator=(const StateTable& o) = delete;

  StateTableEntry* insert(const State& s)
  {
    const std::size_t hash = hashState(s);
    const std::size_t index = hash % NumberOfBuckets;
    return Buckets[index].insert(s, hash);
  }

  StateTableEntry* entry(const State& s)
  {
    const std::size_t hash = hashState(s);
    const std::size_t index = hash % NumberOfBuckets;
    return Buckets[index].find(s, hash);
  }

  StateTableEntry* getOrInsertEntry(const State& s, bool& inserted)
  {
    const std::size_t hash = hashState(s);
    const std::size_t index = hash % NumberOfBuckets;
    StateTableEntry* result = Buckets[index].find(s, hash);
    inserted = result == nullptr;
    if (!result) {
      result = Buckets[index].insert(s, hash);
    }
    return result;
  }

  void clear()
  {
    for (std::size_t i = 0; i < NumberOfBuckets; ++i) {
      Buckets[i].clear();
    }
  }

private:
  std::size_t hashState(const State& s) const
  {
    const std::size_t numberOfCrates = s.numberOfCrates();
    std::size_t result = Randoms[s.playerPosition()];
    for (std::size_t i = 0; i < numberOfCrates; ++i) {
      result ^= Randoms[s.crateAt(i)];
    }
    return result;
  }
};

#endif
