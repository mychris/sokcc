# SPDX-License-Identifier: MIT

EXECUTABLE := sokcc
GTEST_MAIN := $(EXECUTABLE)_gtest
GBENCH_MAIN := $(EXECUTABLE)_gbench
SRC != find src/ -name '*.cc' -printf '%f\n'
SRC_GTEST != find src/ unittests/ -name '*.cc' -a -not -name 'main.cc' -printf '%f\n'
SRC_GBENCH != find src/ benchmarks/ -name '*.cc' -a -not -name 'main.cc' -printf '%f\n'
OBJ = ${SRC:.cc=.o}
OBJ_GTEST = ${SRC_GTEST:.cc=.o}
OBJ_GBENCH = ${SRC_GBENCH:.cc=.o}

CXX = clang++

CPPFLAGS = -I'include/' -DENABLE_ASSERTIONS=1

CXXFLAGS = -std=c++17 -ggdb3 -O0
CXXFLAGS += -MMD -MP
CXXFLAGS += -Wall -Wextra \
	    -Wformat -Wreturn-type -Wstrict-aliasing \
	    -Wcast-qual -Wcast-align -Wconversion \
	    -Wwrite-strings -Wsign-conversion \
	    -Wshadow \
	    -pedantic \
	    -fno-exceptions \
	    -fno-rtti \
	    -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract \
	    -fsanitize=leak \
	    -fsanitize=undefined \
	    -fno-sanitize-recover=all

LDFLAGS =

LDLIBS =
LDLIBS_GTEST = ${LDLIBS} -lgtest_main -lgtest
LDLIBS_GBENCH = ${LDLIBS} -lbenchmark

.PATH: src unittests benchmarks
VPATH = src unittests benchmarks

.IMPSRC = $<
.TARGET = $@
.MEMBER = $%
.ALLSRC = $^

GTEST_FILTER ?= *
GTEST_COLOR ?= auto

.DEFAULT: all
all: $(EXECUTABLE)
gtest: $(GTEST_MAIN)
gbench: $(GBENCH_MAIN)

$(EXECUTABLE): $(OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $(EXECUTABLE) $(.ALLSRC) $(LDLIBS)

$(GTEST_MAIN): $(OBJ_GTEST)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $(GTEST_MAIN) $(.ALLSRC) $(LDLIBS_GTEST)

$(GBENCH_MAIN): $(OBJ_GBENCH)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $(GBENCH_MAIN) $(.ALLSRC) $(LDLIBS_GBENCH)

.cc.o:
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $(.IMPSRC)

-include $(SRC:.cc=.d)

check: $(GTEST_MAIN)
	GTEST_COLOR=${GTEST_COLOR} ./$(GTEST_MAIN) --gtest_color='${GTEST_COLOR}' --gtest_filter='${GTEST_FILTER}'

clean:
	rm -rf *.o *.d $(EXECUTABLE) $(GTEST_MAIN) $(GBENCH_MAIN)

.PHONY: all clean check
