/* SPDX-License-Identifier: MIT */
#include "level.h"
#include "dead_spot.h"
#include "tile_marking.h"
#include <benchmark/benchmark.h>
#include <string>

const std::string levelString = "  ###############################\n"
                                "  #                             #\n"
                                "  #$.************************** #\n"
                                "  # *                         * #\n"
                                "  # * ##############  ####### * #\n"
                                "  # * #           ########  # * #\n"
                                "  # * # *********           # * #\n"
                                "  # * # *       **********  # * #\n"
                                "  # * # * #####          * ## * #\n"
                                " ## * # * #  ########### * # .* #\n"
                                " # .* # * #            # * # ** #\n"
                                " # ** # * # $########  # * # *. #\n"
                                " # *. # * # $     ..$ ## * # * ##\n"
                                " # * ## * # # *****.$ ## * # * # \n"
                                " # * #  * # # *...#.$ ## * # * # \n"
                                " # * # ** # # **@$$ $ ## * # * # \n"
                                " # * # *  # # *...#.$ ## * # * # \n"
                                " # * # * ## # *****.$ ## * # * # \n"
                                " # * # * ## #     ..$ ## * # * # \n"
                                "## * # * #  $$######$ ## * # * # \n"
                                "# .* # * #            ## * # * # \n"
                                "# *# # * ######### #  ## * # * # \n"
                                "# *# # *         $ ##### * # * # \n"
                                "# *# $ .**********.#   # * # * # \n"
                                "# *### .         $   $ # * # * # \n"
                                "# *  # .############# ## . #** # \n"
                                "# *.$#### $ $ $ $ $ $  ##### * # \n"
                                "# .                   $      * # \n"
                                "# ***************************.$# \n"
                                "#                              # \n"
                                "################################ \n";

template <class Q>
void BenchmarkStaticDeadTileMarking(benchmark::State &state)
{
  Level level = Level::parseFromString(levelString);
  for (auto _ : state) {
    Q checker;
    checker.configureFor(level);
    benchmark::DoNotOptimize(checker);
  }
}

BENCHMARK(BenchmarkStaticDeadTileMarking<StaticDeadTileNoOpCheckingAlgorithm>)
  ->Name("BenchmarkStaticDeadTileMarking_StaticDeadTileNoOpCheckingAlgorithm");
BENCHMARK(BenchmarkStaticDeadTileMarking<StaticDeadTileCheckCornersCheckingAlgorithm>)
  ->Name("BenchmarkStaticDeadTileMarking_StaticDeadTileCheckCornersCheckingAlgorithm");
BENCHMARK(BenchmarkStaticDeadTileMarking<StaticDeadTileSinglePushCheckingAlgorithm>)
  ->Name("BenchmarkStaticDeadTileMarking_StaticDeadTileSinglePushCheckingAlgorithm");
BENCHMARK(BenchmarkStaticDeadTileMarking<StaticDeadTileInspectAllPushesCheckingAlgorithm>)
  ->Name("BenchmarkStaticDeadTileMarking_StaticDeadTileInspectAllPushesCheckingAlgorithm");

BENCHMARK_MAIN();
