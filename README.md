# sokcc

A sokoban puzzle solver.

This sokoban solver is a WIP, but can already solve simple puzzles.
See the `t` directory for some test cases.

The current algorithm is too simple to be able to solve large puzzles.
It uses a breadth first search through the graph of all possible states.

To reduce the search space, static dead spots are identified before solving
the puzzle and those tiles are never considered.

## Input and Output format

The solver uses the XSB format to read in levels.  Levels are stored in XSB files.
Each file can contain multiple levels, each seperated by at least one empty line.

Internally, the LURD format is used to store the computed solution.  The output
format changes from time to time, adjusted to the current needs.

## Dead spots

Tiles, to which a crate should never be pushed, because once pushed to it, there
are no moves which can bring the crate to one of the final storage locations,
are called dead spots.  There are static and dynamic dead spots.

### Static dead spots

Those dead spots are always dead, regardless of the current state (player
and crate positions).  A simple example would be a corner:

    #######
	#    .#
	# @$  #
	#     #
	#######

In the above level, only the upper right corner is not a dead spot.  All the
other corners are dead.  If the crate is pushed onto those, it can never be
pushed away from them again, and the crate can therefore never be pushed to
the final storage location.

A more complex example are alcoves:

       #######
	####     ####
	#     $   . #
	#     @     #
	#############
	
For the above level, not only all corners are dead, but all of the top most
tiles are.  If the crate is pushed to that row, it could maybe be pushed to
the left or right (if it is not pushed into one of the corners), but can never
be pushed down again.  If the storage location would be on one of those top most
tiles, not all of them would be dead.

### Dynamic dead spots

For some states (player and crate positions), additional tiles become dead
because of the create positions:

            #####
    ########    ######
	# $ $@           #

The above state only shows a part of the level.  The tile between the two crates
is a dead spot, only because of the current state.  Regardless if the left crate
is pushed to the right, the right crate is pushed to the left, or a third crate
is pushed between the two.  Any crate pushed to that tile is lost forever and
can never be pushed to any other tile.

## C++ STL

I started without using the STL, implementing required datastructures myself
and using raw pointers everywhere.  In addition, I tried to reduce the number of
virtual function calls to a  minimum.  Since this slowed down development a lot,
I now changed my mind and am using the STL.

I noticed that my concerns about the runtime costs of using the STL were
justified.  The solver is a bit slower now.  But the performance penalty
is still bearable.  I guess it is a better approached to continue using
the STL and benchmark later on to see which datastructures should be
replaced, where to use raw pointers and what function call to
manually de-virtualize.

There are still some remnants because of how I started.

## Todo

* Better visualization
* Lots of code cleanup
* Benchmark datastructures
* Corral identification
* Improve algorithm

## Links

* http://sokobano.de/wiki/index.php?title=Solver
* https://sokoban-solver-statistics.sourceforge.io/statistics/LargeTestSuite/
* https://sourceforge.net/projects/sokoban-solver-statistics/files/
* http://sokoban.org/about_sokoban.php

## Copyright

Copyright © 2022-2023, Christoph Göttschkes.

Released under MIT license.

The XSB files used for testing in the `t` directory are each under their
own license with different copyright holders.  Please see the individual
files.
